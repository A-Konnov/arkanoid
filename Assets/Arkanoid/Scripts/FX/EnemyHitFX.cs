﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHitFX : MonoBehaviour
{
    [SerializeField] private GameObject m_fxPrefab;

    private void OnCollisionEnter(Collision collision)
    {
        foreach (ContactPoint cPoint in collision.contacts)
        {
            if (cPoint.otherCollider.CompareTag("Ball"))
            {
                CreatFX(m_fxPrefab, cPoint);
                break;
            }
        }
    }

    private void CreatFX(GameObject prefab, ContactPoint cPoint)
    {
        Vector3 pos = cPoint.point;
        Quaternion rot = Quaternion.LookRotation(-cPoint.normal);
        Instantiate(prefab, pos, rot, transform); //префаб, позиция, поворот, сделать парентом
    }
}
