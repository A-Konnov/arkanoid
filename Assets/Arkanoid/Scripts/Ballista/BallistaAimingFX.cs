﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BallistaAiming))]
public class BallistaAimingFX : MonoBehaviour
{
    [SerializeField] private Transform m_aimFX;
    [SerializeField] private float m_movementSpeed = 5f;

    private BallistaAiming m_aiming;

    private void Start()
    {
        m_aiming = GetComponent<BallistaAiming>();
    }

    private void Update()
    {
        var targetExist = m_aiming.Target != null;
        
        m_aimFX.gameObject.SetActive(targetExist);

        if(!targetExist)
        {
            return;
        }

        var movementSpeed = m_movementSpeed * Time.deltaTime;
        var newPos = Vector3.Lerp(m_aimFX.transform.position, m_aiming.Target.transform.position, movementSpeed);
        newPos.y = m_aimFX.position.y;
        m_aimFX.position = newPos;
    }

}
