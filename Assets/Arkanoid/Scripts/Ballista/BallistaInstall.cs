﻿using UnityEngine;


public class BallistaInstall : MonoBehaviour
{
    [Header("Points:")]
    [SerializeField] private Transform[] m_ballistaPoints;

    [Header("Prefabs:")]
    [SerializeField] private GameObject m_woodBallista;
    [SerializeField] private GameObject m_woodSteelBallista;
    [SerializeField] private GameObject m_steelBallista;
    private float m_duration = 5f;

    public float Duration
    {
        set { m_duration = value; }
    }

    public void InstallWoodBallista()
    {
        InstBallista(m_woodBallista);
    }

    public void InstallWoodSteelBallista()
    {
        InstBallista(m_woodSteelBallista);
    }

    public void InstallSteelBallista()
    {
        InstBallista(m_steelBallista);
    }

    private void InstBallista(GameObject ballista_prefab)
    {
        DestroyBallista();
        int randomPoints = Random.Range(0, m_ballistaPoints.Length);
        var ballista = Instantiate(ballista_prefab, m_ballistaPoints[randomPoints].transform.position, m_ballistaPoints[randomPoints].transform.rotation, gameObject.transform);
        Destroy(ballista, m_duration);
    }

    private void DestroyBallista()
    {
        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            if (gameObject.transform.GetChild(i).gameObject.GetComponent<Animator>())
            {
                Destroy(transform.GetChild(i).gameObject);
            }
        }
    }
}
