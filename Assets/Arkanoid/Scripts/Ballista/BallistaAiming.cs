﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallistaAiming : MonoBehaviour
{
    [Header("Reference:")]
    [SerializeField] private Transform m_tower;

    [Header("Settings:")]
    [SerializeField] private float m_rotationSpeed;

    private GameObject m_target;

    public GameObject Target
    {
        get { return m_target; }
        set { m_target = value; }
    }

    public Transform Tower
    {
        get { return m_tower; }
        set { m_tower = value; }
    }

    private void Update()
    {
        if(Target == null)
        {
            FindClosestEnemyTarget();
            return;
        }

        RotateTower(Tower);
    }

    private void RotateTower(Transform tower)
    {
        var lookDir = Target.transform.position - tower.position;
        lookDir.y = 0;

        var targetRot = Quaternion.LookRotation(lookDir);
        var rotSpeed = m_rotationSpeed * Time.deltaTime;

        Tower.rotation = Quaternion.Lerp(Tower.rotation, targetRot, rotSpeed);
    }

    private void FindClosestEnemyTarget()
    {
        var enemies = GameObject.FindGameObjectsWithTag("Enemy");
        if (enemies.Length == 0)
        {
            return;
        }

        var smallestDist = float.MaxValue;

        foreach(var enemy in enemies)
        {
            var dist = (enemy.transform.position - Tower.position).sqrMagnitude;

            if(dist < smallestDist)
            {
                Target = enemy;
                smallestDist = dist;
            }
        }
    }
}
