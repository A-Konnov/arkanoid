﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BallistaAiming))]
public class BallistaShooting : MonoBehaviour
{
    [Header("Reference:")]
    [SerializeField] private GameObject m_ammo;
    [SerializeField] private Transform m_firePoint;
    [SerializeField] private Animator m_animator;

    [Header("Setting:")]
    [SerializeField] private float m_shotForce = 300f;
    [SerializeField] private float m_shootingInterval = 1f;
    [SerializeField] private LayerMask m_shootingMask;
    [SerializeField] private float m_minShootingAngle = 5f;


    private BallistaAiming m_aiming;
    private float m_shoottingTimer;
  
    private void Start()
    {
        m_aiming = GetComponent<BallistaAiming>();
    }

    private void Update()
    {
        UpdateTimer();

        if (m_aiming.Target == null)
        {
            return;
        }

        if (m_shoottingTimer <= 0 && IsRay() && IsShootingAngle())
        {
            m_animator.SetTrigger("Shot");
            ResetTimer();
        }
    }

    private bool IsShootingAngle()
    {
        var lookDir = m_aiming.Target.transform.position - m_aiming.Tower.position;
        lookDir.y = 0;
        var targetRot = Quaternion.LookRotation(lookDir);
        var angle = Quaternion.Angle(m_aiming.Tower.rotation, targetRot);

        if (angle > m_minShootingAngle)
        {
            return false;
        }
        return true;
    }

    private bool IsRay()
    {
        RaycastHit hit;
        var target = m_aiming.Target.transform.position - transform.position;
        Ray ray = new Ray(transform.position, target);

        if (Physics.Raycast(ray, out hit, 50f, m_shootingMask.value))
        {
            return false;
        }
        return true;
    }

    private void ResetTimer()
    {
        m_shoottingTimer = m_shootingInterval;
    }

    private void UpdateTimer()
    {
        m_shoottingTimer -= Time.deltaTime;
    }

    //Called from animation "Shoot"
    private void MakeShot()
    {
        var shotPos = m_firePoint.position;
        var shotRot = m_firePoint.rotation;

        var ammoClone = Instantiate(m_ammo, shotPos, shotRot);
        var rb = ammoClone.GetComponent<Rigidbody>();

        rb.isKinematic = false;
        rb.AddForce(m_firePoint.forward * m_shotForce, ForceMode.Impulse);
    }
}
