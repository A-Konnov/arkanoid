﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BallistaAiming))]
public class SetTargetByClick : MonoBehaviour
{
    [SerializeField] private LayerMask m_mask;

    private BallistaAiming m_aiming;

    private void Start()
    {
        m_aiming = GetComponent<BallistaAiming>();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 50f, m_mask.value))
            {
                m_aiming.Target = hit.transform.gameObject;
            }
        }
    }

}
