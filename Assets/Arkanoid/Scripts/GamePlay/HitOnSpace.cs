﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitOnSpace : MonoBehaviour
{
    [SerializeField] private float m_maxTime = 1f;
    [SerializeField] private Collider[] m_colliders;

    private float m_timer;

    private void Start()
    {
        SetEnableColliders(false);
    }

    private void Update()
    {
        var isTimerEnd = m_timer < 0;
        if (isTimerEnd && Input.GetKeyDown(KeyCode.Space))
        {
            m_timer = m_maxTime;
        }

        SetEnableColliders(!isTimerEnd);

        m_timer -= Time.deltaTime;
    }


    private void SetEnableColliders(bool enable)
    {
        foreach (var collider in m_colliders)
        {
            collider.enabled = enable;
        }
    }

}
