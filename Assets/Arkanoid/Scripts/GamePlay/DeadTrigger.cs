﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeadTrigger : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        var balls = GameObject.FindGameObjectsWithTag("Ball");
        if(balls.Length > 1)
        {
            Destroy(other.gameObject);
        }
        else
        {
            GameController.PlayerHealth--;
            GameController.StopGame = true;
        }
    }
}
