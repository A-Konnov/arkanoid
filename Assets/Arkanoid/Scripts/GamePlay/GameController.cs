﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class GameController : MonoBehaviour
{
    public static int PlayerHealth;
    public static int StarCount;
    public static bool IsVictory;
    public static bool StopGame;
    public static Action<bool> EndOfGameEvent;

    private float m_timer = 1f;
    private int m_levelStars;

    private void Awake()
    {
        ResetGameState();
    }

    private void Update()
    {
        if (Time.time < m_timer)
        {
            return;
        }

        m_timer = Time.time + 1f;

        var enemies = GameObject.FindGameObjectsWithTag("Enemy");
        var isEndOfGame = PlayerHealth <= 0 || enemies.Length == 0;

        if(isEndOfGame)
        {
            if (PlayerHealth <= 0) IsVictory = false;
            else if (enemies.Length == 0) IsVictory = true;


            if(EndOfGameEvent != null)
            {
                EventManager.Invoke("win");
                EventManager.Invoke("winGold");

                RecordNumberStars();
                EndOfGameEvent.Invoke(IsVictory);
                enabled = false;
            }          
        }
    }

    private void ResetGameState()
    {
        IsVictory = false;
        StopGame = true;
        PlayerHealth = 5;
        StarCount = 0;
    }    

    private void RecordNumberStars()
    {
        m_levelStars = PlayerPrefs.GetInt("LevelStars" + SceneManager.sceneCount);
        if (m_levelStars < StarCount)
        {
            PlayerPrefs.SetInt("LevelStars" + SceneManager.sceneCount, StarCount);
        }
    }
}
