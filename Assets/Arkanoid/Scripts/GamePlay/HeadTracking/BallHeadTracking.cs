﻿using UnityEngine;

public class BallHeadTracking : AHeadTracking
{
    protected override Transform FindTarget()
    {
        return FindClosestBall();
    }
}
