﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformHeadTracking : AHeadTracking
{
    [SerializeField] private float m_minDistanceToBall = 1f;

    protected override Transform FindTarget()
    {
        var platform = FindPlatform();
        var ball = FindClosestBall();

        if (ball != null)
        {
            var distanceToBall = (ball.transform.position - m_head.transform.position).sqrMagnitude;

            if(distanceToBall < Mathf.Pow(m_minDistanceToBall, 2f))
            {
                return ball;
            }
        }

        return platform;
    }
}
