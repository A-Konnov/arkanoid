﻿using UnityEngine;

public abstract class AHeadTracking : MonoBehaviour
{
    [Header("Settings:")]
    [SerializeField] private float m_lookSpeed = 5f;
    [SerializeField] private float m_minDot = 0.3f;

    [Header("References:")]
    [SerializeField] protected Transform m_head;

    private Vector3 m_defaultLookDir;
    private Quaternion m_prevLookDir;

    protected abstract Transform FindTarget();

    private void Start()
    {
        m_defaultLookDir = m_head.forward;
        m_prevLookDir = Quaternion.LookRotation(m_defaultLookDir);
    }

    //IMPORTANT!!! Use LateUpdate, becouse we have to ovverade animation!
    private void LateUpdate()
    {
        var target = FindTarget();

        var lookDir = m_defaultLookDir;

        if (target != null)
        {
            lookDir = target.position - m_head.position;
        }

        var rot = Quaternion.LookRotation(lookDir);
        m_head.rotation = Quaternion.Lerp(m_prevLookDir, rot, Time.deltaTime * m_lookSpeed);
        m_prevLookDir = m_head.rotation;
    }

    protected Transform FindPlatform()
    {
        var platform = GameObject.FindGameObjectWithTag("Platform");

        if (!CheckDot(platform.transform))
        {
            return null;
        }

        return platform.transform;
    }

    protected Transform FindClosestBall()
    {
        var balls = GameObject.FindGameObjectsWithTag("Ball");

        Transform closestBall = null;
        float closestSqrDistance = float.MaxValue;

        foreach (var ball in balls)
        {
            if(!CheckDot(ball.transform))
            {
                continue;
            }

            var distance = (ball.transform.position - m_head.position).sqrMagnitude;

            if(distance <= closestSqrDistance)
            {
                closestSqrDistance = distance;
                closestBall = ball.transform;
            }
        }

        return closestBall;
    }

    protected bool CheckDot(Transform target)
    {
        var dirToBall = target.transform.position - m_head.position;
        var dot = Vector3.Dot(m_defaultLookDir.normalized, dirToBall.normalized);

        if (dot < m_minDot)
        {
            return false;
        }

        return true;
    }
}
