﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody), typeof(Collider))] //запрещает удалять компоненты или автоматом добавляет компоненты в инспекторе
public class ObjectsHealth : MonoBehaviour
{
    [SerializeField] private float m_health = 1;
    [SerializeField] private GameObject m_fx;

    private Rigidbody m_rb;
    private float m_prevHealth;

    public UnityEvent OnDieEvent = new UnityEvent();

    public float Health//даем доступ к health
    {
        get { return m_health; }
        set { m_health = value; }
    }
    
    protected virtual void Start()
    {
        m_rb = GetComponent<Rigidbody>();  //ссылки к компонентам
        m_prevHealth = Health;
    }

    void LateUpdate()
    {
        if (transform.position.y > 1f) //не даем уснуть объектам выше 1
        {
            m_rb.WakeUp();
        }

        if (m_prevHealth == Health)
        {
            return;
        }

        OnHealthChanged();

        if (Health <= 0)
        {
            if (m_fx != null)
            {
                var fx = Instantiate(m_fx, gameObject.transform.position, gameObject.transform.rotation);
                Destroy(fx, 3f);
            }
            Destroy(gameObject);
            OnDieEvent.Invoke();
        }
    }

    protected virtual void OnHealthChanged()
    {

    }
}
