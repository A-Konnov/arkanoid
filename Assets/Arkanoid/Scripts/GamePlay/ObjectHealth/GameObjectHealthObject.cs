﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectHealthObject : ObjectsHealth
{
    [SerializeField] private GameObject[] m_objects;

    protected override void Start()
    {
        base.Start();

        SetObject();
    }

    private void SetObject()
    {
        if (m_objects.Length > 0)
        {
            int index = m_objects.Length - (int) Health;

            if (index >= 0 && index < m_objects.Length)
            {
                for (int i = 0; i < m_objects.Length; i++)
                {
                    m_objects[i].SetActive(i == index);
                }
            }
        }
    }

    protected override void OnHealthChanged()
    {
        base.OnHealthChanged();

        SetObject();
    }
}
