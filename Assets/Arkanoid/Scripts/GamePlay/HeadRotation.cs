﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadRotation : MonoBehaviour
{
    [SerializeField] private GameObject m_headObject;
    [SerializeField] private float m_lookSmoothness = 2f;
    [SerializeField] private float m_angleDot = 0.3f;
    [SerializeField] private float m_distanceToBall = 5f;

    private GameObject m_targetObject;
    private Quaternion m_targetRotation;
    private Quaternion m_defaultRotation;
    private GameObject[] m_targets;
    private float m_targetSqrLength;
    private Vector3 m_forward;
    private Vector3 m_targetVector;
    private Quaternion m_currHeadRotation;

    public enum Target { Ball, Platform, BallAndPlatform }

    [SerializeField] private Target m_target;

    private void Start()
    {
        m_defaultRotation = m_headObject.transform.rotation;
        m_forward = transform.TransformDirection(Vector3.forward);
    }

    private void Update()
    {
        m_currHeadRotation = m_headObject.transform.rotation;
    }

    private void LateUpdate()
    {
        NearestTarget(m_target);
        RotationToTarget();
    }

    private void NearestTarget(Target targetObject)
    {
        m_targets = null;

        switch (targetObject)
        {
            case Target.Ball:
                m_targets = GameObject.FindGameObjectsWithTag("Ball");
                break;
            case Target.Platform:
                m_targets = GameObject.FindGameObjectsWithTag("Platform");
                break;
            case Target.BallAndPlatform:
                m_targets = GameObject.FindGameObjectsWithTag("Ball");
                if (m_targets.Length != 0)
                {
                    if (IsDistance())
                    {
                        break;
                    }
                }
                m_targets = null;
                m_targets = GameObject.FindGameObjectsWithTag("Platform");
                break;
            default:
                return;
        }
   
        if (m_targets.Length != 0)
        {
            m_targetObject = m_targets[0]; 
            m_targetVector = m_targetObject.transform.position - m_headObject.transform.position; 
            m_targetSqrLength = m_targetVector.sqrMagnitude; 

            foreach (GameObject target in m_targets) 
            {
                var tempTargetVector = target.transform.position - m_headObject.transform.position;
                var tempTargetSqrLength = tempTargetVector.sqrMagnitude;
                var tempTargetDot = Vector3.Dot(m_forward.normalized, tempTargetVector.normalized);

                if (tempTargetSqrLength < m_targetSqrLength && tempTargetDot > m_angleDot)
                {
                    m_targetSqrLength = tempTargetSqrLength;
                    m_targetVector = tempTargetVector;
                    m_targetObject = target;
                }
            }
        }        
    }

    private bool IsDistance()
    {
        foreach (GameObject target in m_targets)
        {
            var dist = Vector3.Distance(target.transform.position, m_headObject.transform.position);
            if (dist < m_distanceToBall)
            {
                return true;
            }
        }
        return false;
    }

    private void RotationToTarget()
    {
        if (m_targetObject)
        {
            var targetDot = Vector3.Dot(m_forward.normalized, m_targetVector.normalized);
            if (targetDot > m_angleDot)
            {
                m_targetRotation = Quaternion.LookRotation(m_targetVector); 
            }
            else
            {
                m_targetRotation = m_defaultRotation;
            }
        }
        else
        {
            m_targetRotation = m_defaultRotation;
        }

        m_headObject.transform.rotation = Quaternion.Lerp(m_currHeadRotation, m_targetRotation, Time.deltaTime * m_lookSmoothness);
    }
}
