﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationToTarget : MonoBehaviour
{
    public enum Target { Ball, Platform, BallAndPlatform }

    [SerializeField] private Transform m_rotationObject;
    [SerializeField] private float m_rotationSmoothness = 2f;
    [SerializeField] private float m_angleDot = 0.3f;
    [SerializeField] private float m_distanceToTarget = 5f;
    [SerializeField] private Target m_targetType;

    private List<GameObject> m_targets;
    private Quaternion m_defaultRotation;
    private Quaternion m_currRotation;
    private Vector3 m_defaultForward;
    private GameObject m_target;
    private float m_targetDistance;

    private void Start()
    {
        m_targets = new List<GameObject>();
        m_defaultRotation = m_rotationObject.rotation;
    }

    private void Update()
    {
        m_currRotation = m_rotationObject.rotation;
    }

    private void LateUpdate()
    {
        SelectTarget(m_targetType);
        if (IsDot())
        {
            NearestTarget();
            Rotation(m_target.transform.position);
        }
        else
        {
            Rotation(m_defaultForward);
        }
        m_targets.Clear();

    }

    private void SelectTarget(Target targetObject)
    {
        m_targets.Clear();

        switch (targetObject)
        {
            case Target.Ball:
                m_targets.AddRange(GameObject.FindGameObjectsWithTag("Ball"));
                m_defaultForward = new Vector3(0, 0, 1);
                break;
            case Target.Platform:
                m_targets.AddRange(GameObject.FindGameObjectsWithTag("Platform"));
                m_defaultForward = new Vector3(0, 0, -1);
                break;
            case Target.BallAndPlatform:
                m_targets.AddRange(GameObject.FindGameObjectsWithTag("Ball"));
                m_defaultForward = new Vector3(0, 0, -1);
                if (IsDistance() && IsDot())
                    break;
                m_targets.Clear();
                m_targets.AddRange(GameObject.FindGameObjectsWithTag("Platform"));
                m_defaultForward = new Vector3(0, 0, -1);
                break;
            default:
                return;
        }
    }

    private bool IsDistance()
    {
        if (m_targets.Count != 0)
        {
            foreach (GameObject target in m_targets)
            {
                var dist = Vector3.Distance(target.transform.position, m_rotationObject.position);
                if (dist < m_distanceToTarget)
                {
                    return true;
                }
            }
        }
        return false;
    }

    private bool IsDot()
    {
        if (m_targets.Count != 0)
        {
            for (int i=0; i < m_targets.Count;)
            {
                var toTarget = m_targets[i].transform.position - m_rotationObject.position;
                var dot = Vector3.Dot(m_defaultForward.normalized, toTarget.normalized);
                if (dot < m_angleDot)
                {
                    m_targets.RemoveAt(i);
                }
                else i++;
            }
            if (m_targets.Count != 0)
            {
                return true;
            }
        }
        return false;
    }

    private void NearestTarget()
    {
        m_target = m_targets[0];
        m_targetDistance = Vector3.Distance(m_target.transform.position, m_rotationObject.position);
        foreach (GameObject target in m_targets)
        {
            var currTargetDistance = Vector3.Distance(target.transform.position, m_rotationObject.position);
            if (currTargetDistance < m_targetDistance)
            {
                m_target = target;
                m_targetDistance = currTargetDistance;
            }
        }
    }

    private void Rotation(Vector3 target)
    {
        var targetVector = target - m_rotationObject.position;
        var targetRotation = Quaternion.LookRotation(targetVector);
        m_rotationObject.rotation = Quaternion.Lerp(m_currRotation, targetRotation, Time.deltaTime * m_rotationSmoothness);
    }

}
