﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetBonus : MonoBehaviour
{
    [SerializeField] private GameObject[] m_bonuses;
    private ObjectsHealth m_health;

    private void Start()
    {
         m_health = GetComponent<ObjectsHealth>();
    }


    private void Update()
    {
        if (m_health.Health <= 0)
        {
            BonusSet();
        }
    }

    private void BonusSet()
    {
        int isBonus = Random.Range(0, 2);
        if (isBonus == 0)
        {
            BonusInst();
        }
    }

    private void BonusInst()
    {
        int numberBonus = Random.Range(0, m_bonuses.Length);
        Instantiate(m_bonuses[numberBonus], gameObject.transform.position, new Quaternion(0, 0, 0, 0));
    }
}
