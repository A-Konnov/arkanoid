﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "BallAssets/BallKamikaze", fileName = "NewBallKamikaze")]
public class BallKamikaze : BallBehaviourAsset
{
    public override void EnableBehaviour(GameObject ball)
    {
        base.EnableBehaviour(ball);

        var attack = ball.GetComponent<BallAttack>();
        attack.OnDamageEvent.AddListener(OnDamage); //подписались
    }

    public override void DisableBehaviour(GameObject ball)
    {
        base.DisableBehaviour(ball);

        var attack = ball.GetComponent<BallAttack>();
        attack.OnDamageEvent.RemoveListener(OnDamage); //отписались
    }
    private void OnDamage(GameObject target)
    {
        var platformShooting = FindObjectOfType<PlatformShooting>();
        platformShooting.ResetBallOnce();
    }
}
