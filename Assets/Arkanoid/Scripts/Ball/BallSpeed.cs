﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class BallSpeed : MonoBehaviour
{
    [SerializeField] private float m_targetSpeed = 15f;
    private Rigidbody m_rb;

    public float CurrSpeed
    {
        get { return m_targetSpeed; }
        set { m_targetSpeed = value; }
    }
    
    void Start()
    {
        m_rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        float sqrSpeed = m_rb.velocity.sqrMagnitude; //без квадратного корня

        if (sqrSpeed != Mathf.Pow(m_targetSpeed, 2)) //Pow возвести в степень 2
        {
            m_rb.velocity = m_rb.velocity.normalized * m_targetSpeed; //нормализуем вектор и умножаем на заданную скорость
        }
    }
}
