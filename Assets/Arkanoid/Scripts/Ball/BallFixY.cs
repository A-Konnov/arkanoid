﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallFixY : MonoBehaviour
{
    private Rigidbody m_rigidbody;

    private void Start()
    {
        m_rigidbody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if (gameObject.transform.position.y <= 0.5f)
        {
            m_rigidbody.constraints = RigidbodyConstraints.FreezePositionY;
        }
    }
}
