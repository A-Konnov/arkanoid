﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "BallAssets/BaseBall", fileName = "NewBaseBall")]
public class BallBehaviourAsset : ScriptableObject
{
    [Header("Settings:")]
    [SerializeField] private float m_speed = 15f;
    [SerializeField] private float m_radius = 0.45f;
    [SerializeField] private float m_damage = 1f;

    public virtual void EnableBehaviour(GameObject ball)
    {
        SetSpeed(ball);
        SetRadius(ball);
        SetDamage(ball);
    }

    public virtual void DisableBehaviour(GameObject ball)
    {

    }

    private void SetSpeed(GameObject ball)
    {
        var ballSpeed = ball.GetComponent<BallSpeed>();

        if(ballSpeed != null)
        {
            ballSpeed.CurrSpeed = m_speed;
        }
    }

    private void SetRadius (GameObject ball)
    {
        var collider = ball.GetComponent<SphereCollider>();

        if (collider != null)
        {
            collider.radius = m_radius;
        }
    }

    private void SetDamage (GameObject ball)
    {
        var attack = ball.GetComponent<BallAttack>();

        if (attack != null)
        {
            attack.Damage = m_damage;
        }
    }
}
