﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallBehaviourExecuter : MonoBehaviour
{
    [SerializeField] private EBallType m_defaultType;
    [SerializeField] private BallBehaviourAsset[] m_assets;
    [SerializeField] private GameObject[] m_views;

    private void Start()
    {
        SetBallType(m_defaultType);
    }

    public void SetBallType(EBallType type)
    {
        var typeIndex = (int)type;

        for (int i = 0; i < m_views.Length; i++)
        {
            m_views[i].SetActive(typeIndex == i);

            if (i == typeIndex)
            {
                m_assets[i].EnableBehaviour(gameObject);
            }
            else
            {
                m_assets[i].DisableBehaviour(gameObject);
            }
        }
    }

    public void SetBall()
    {

    }


}
