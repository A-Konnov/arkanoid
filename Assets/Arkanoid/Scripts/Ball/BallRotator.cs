﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class BallRotator : MonoBehaviour
{
    [SerializeField] private Transform m_parent;
    [SerializeField] private float m_rotSpeed;

    private Rigidbody m_rb;

    private void Start()
    {
        m_rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        var rotateAxis = Vector3.Cross(m_rb.velocity.normalized, Vector3.down);
        var ballSpeed = m_rb.velocity.magnitude;
        var rotSpeed = ballSpeed * Time.deltaTime * m_rotSpeed;

        m_parent.RotateAround(m_parent.position, rotateAxis, rotSpeed);
    }
}
