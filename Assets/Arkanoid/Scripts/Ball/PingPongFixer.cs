﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))] 
public class PingPongFixer : MonoBehaviour
{
    [SerializeField] private float m_pingPongEpsilon = 0.02f;
    [SerializeField] private int m_maxPingPongFrames = 600;
    [SerializeField] private float m_pushForce = 5;

    private Rigidbody m_rb;
    private Vector3 m_prevPos;
    private int m_pingPongCounter;

    private void Start()
    {
        m_prevPos = transform.position;
        m_rb = GetComponent<Rigidbody>();
    }


    private void Update()
    {
        if(Mathf.Abs(transform.position.z - m_prevPos.z) <= m_pingPongEpsilon)
        {
            m_pingPongCounter++;
        }
        else
        {
            m_pingPongCounter = 0;
        }

        if(m_pingPongCounter >= m_maxPingPongFrames)
        {
            m_rb.AddForce(Vector3.forward * m_rb.mass * m_pushForce, ForceMode.Impulse);
        }

        m_prevPos = transform.position;
    }
}
