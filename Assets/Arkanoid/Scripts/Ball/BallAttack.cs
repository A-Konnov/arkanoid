﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameObjectEvent : UnityEvent<GameObject> { }

public class BallAttack : MonoBehaviour
{
    [SerializeField] private float m_damage = 1;
    public GameObjectEvent OnDamageEvent = new GameObjectEvent();

    public float Damage
    {
        get { return m_damage; }
        set { m_damage = value; }
    }

    private void OnCollisionEnter(Collision collision)
    {
        TrySetDamage(collision.gameObject);
    }

    private void TrySetDamage(GameObject gameObject)
    {
        var health = gameObject.GetComponent<ObjectsHealth>();

        if (health != null)
        {
            health.Health -= m_damage;
            OnDamageEvent.Invoke(gameObject);
        }
    }
}
