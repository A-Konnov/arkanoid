﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Image))]
public class SetHorizontalAxisBtn : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField] private InputSettings m_settings;

    [SerializeField, Range(-1, 1)] private float m_axis;

    public UnityEvent OnDown = new UnityEvent();
    public UnityEvent OnUp = new UnityEvent();

    private bool m_isPressed;

    private void Start()
    {
        SetInputType();
        m_settings.OnInputChanged.AddListener(SetInputType);
    }

    private void OnDestroy()
    {
        m_settings.OnInputChanged.RemoveListener(SetInputType);
    }

    private void Update()
    {
        CustomInput.HorizontalAxis = m_isPressed ? m_axis : 0; // если нажато, то m_axis иначе 0
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        m_isPressed = true;
        OnDown.Invoke();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        m_isPressed = false;
        OnUp.Invoke();
    }

    private void SetInputType()
    {
        gameObject.SetActive(m_settings.CurrType == EInputType.UIButtons);
    }
}
