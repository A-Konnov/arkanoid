﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InstallWall : MonoBehaviour
{
    [Header("Points")]
    [SerializeField] private Transform[] m_wallPoints;
    [Header("Prefabs")]
    [SerializeField] private GameObject[] m_wallWoodPrefabs;
    [SerializeField] private GameObject[] m_wallStonePrefabs;
    [SerializeField] private GameObject[] m_wallSteelPrefabs;
    [Space]
    [Header("Propertys")]
    [SerializeField] private float m_delay = 5;

    public void InstallWoodWall()
    {
        InstWall(m_wallWoodPrefabs);
    }

    public void InstallStoneWall()
    {
        InstWall(m_wallStonePrefabs);
    }

    public void InstallSteelWall()
    {
        InstWall(m_wallSteelPrefabs);
    }


    public void InstallAllWoodWall()
    {
        StartCoroutine(InstCoroutineWall(m_wallWoodPrefabs));
    }

    public void InstallAllStoneWall()
    {
        StartCoroutine(InstCoroutineWall(m_wallStonePrefabs));
    }

    public void InstallAllSteelWall()
    {
        StartCoroutine(InstCoroutineWall(m_wallSteelPrefabs));
    }


    private IEnumerator InstCoroutineWall(GameObject[] wall)
    {
        var delay = new WaitForSeconds(m_delay);
        for (int i = 0; i < m_wallPoints.Length; i++)
        {
            if (TrueInst(i))
            {
                yield return delay;
                InstWallType(wall, i);
            }
        }
    }


    //  Удаляем весь забор
    public void DestroyWall()
    {
        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            if (gameObject.transform.GetChild(i).gameObject.GetComponent<Rigidbody>())
            {
                Destroy(transform.GetChild(i).gameObject);
            }
        }
    }

    //Устанавливаем один забор
    private void InstWall(GameObject[] wall)
    {
        for (int i = 0; i < m_wallPoints.Length; i++)
        {
            if (TrueInst(i))
            {
                InstWallType(wall, i);
                return;
            }
        }
    }

    //Определяем тип забора в зависимости от расположения (не нравится способ)
    private void InstWallType(GameObject[] wall, int i)
    {
        if (i == 0)
            Instantiate(wall[0], m_wallPoints[i].transform.position, m_wallPoints[i].transform.rotation, gameObject.transform);
        else if (i == m_wallPoints.Length-1)
            Instantiate(wall[2], m_wallPoints[i].transform.position, m_wallPoints[i].transform.rotation, gameObject.transform);
        else Instantiate(wall[1], m_wallPoints[i].transform.position, m_wallPoints[i].transform.rotation, gameObject.transform);
    }

    //Проверяем, можно ли установить забор на позиции i
    private bool TrueInst(int i)
    {
        for (int j = 0; j < gameObject.transform.childCount; j++)
        {
            if (gameObject.transform.GetChild(j).gameObject.GetComponent<Rigidbody>())
            {
                if (Vector3.Distance(m_wallPoints[i].position, gameObject.transform.GetChild(j).gameObject.transform.position) < 0.1f)
                    return false;
            }
        }
        return true;
    }


    // ********* Запил на visible
    //[SerializeField] private GameObject[] m_walls;
    //[SerializeField] private Toggle m_toggle;
    //private bool m_currPositionActive;

    //public void OnClick()
    //{        
    //    if (!m_toggle.isOn)
    //    {
    //        TurnOffWalls();
    //        TurnOnWalls();
    //    }
    //    else
    //    {
    //        TurnOnWallsOneByOne();
    //    }
    //}

    //private void TurnOnWallsOneByOne()
    //{
    //    for (int position = 0; position < gameObject.transform.childCount; position++)
    //    {
    //        m_currPositionActive = false;
    //        for (int typeWall = 0; typeWall < m_walls.Length; typeWall++)
    //        {
    //            if (m_walls[typeWall].transform.GetChild(position).gameObject.activeSelf)
    //            {
    //                m_currPositionActive = true;
    //            }
    //        }
    //        if (!m_currPositionActive)
    //        {
    //            gameObject.transform.GetChild(position).gameObject.SetActive(true);
    //            return;
    //        }
    //    }
    //}

    //private void TurnOnWalls()
    //{
    //    foreach (Transform child in transform)
    //    {
    //        child.gameObject.SetActive(true);
    //    }
    //}

    //public void TurnOffWalls()
    //{
    //    for (int i = 0; i < m_walls.Length; i++)
    //    {
    //        foreach (Transform child in m_walls[i].transform)
    //        {
    //            child.gameObject.SetActive(false);
    //        }
    //    }
    //}
}
