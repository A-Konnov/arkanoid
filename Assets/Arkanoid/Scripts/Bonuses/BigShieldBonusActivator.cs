﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigShieldBonusActivator : ABonusActivator
{
    public override void Activate(GameObject target)
    {
        var bonus = target.AddComponent<BigShieldBonus>();
        bonus.Duration = m_duration;
        bonus.Infinity = m_infinity;


    }

}
