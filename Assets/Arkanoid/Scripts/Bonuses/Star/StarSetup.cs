﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarSetup : MonoBehaviour
{
    [SerializeField] private float m_timeDestroy = 10f;

    private void Start()
    {
        StartCoroutine(Destroy());
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Platform"))
        {
            PickUp();
        }
    }

    private void OnMouseDown()
    {
        PickUp();
    }

    private void PickUp()
    {
        EventManager.Invoke("colStars");
        GameController.StarCount++;
        Destroy(gameObject);
    }

    private IEnumerator Destroy()
    {
        yield return new WaitForSeconds(m_timeDestroy);
        Destroy(gameObject);
    }
}
