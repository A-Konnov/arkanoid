﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedBonus : ABonus
{
    public float SpeedMod { get; set; }

    protected override void Activate()
    {
        var platformSpeed = GetComponent<APlatformMovement>();
        platformSpeed.MovementSpeed = platformSpeed.BaseSpeed * SpeedMod;
    }

    protected override void Deactivate()
    {
        var platformSpeed = GetComponent<APlatformMovement>();
        platformSpeed.MovementSpeed = platformSpeed.BaseSpeed;
    }
}
