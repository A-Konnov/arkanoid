﻿using UnityEngine;

public abstract class ABonusActivator : MonoBehaviour
{
    [SerializeField] protected float m_duration = 3f;
    [SerializeField] protected bool m_infinity;

    public abstract void Activate(GameObject target);

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Platform"))
        {
            Activate(other.gameObject);
            Destroy(gameObject);
        }
    }

    private void OnMouseDown()
    {
        var platform = GameObject.FindGameObjectWithTag("Platform");

        if(platform != null)
        {
            Activate(platform);
            Destroy(gameObject);
        }
    }
}
