﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallShieldBonusActivator : ABonusActivator
{
    public override void Activate(GameObject target)
    {
        var bonus = target.AddComponent<SmallShieldBonus>();
        bonus.Duration = m_duration;
        bonus.Infinity = m_infinity;


    }

}
