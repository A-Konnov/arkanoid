﻿using UnityEngine;

public class BonusBallActivator : ABonusActivator
{
    [SerializeField] private EBallType m_ballType;

    public override void Activate(GameObject target)
    {
        var bonus = target.AddComponent<BonusBall>();
        bonus.Duration = m_duration;
        bonus.BallType = m_ballType;
    }
}
