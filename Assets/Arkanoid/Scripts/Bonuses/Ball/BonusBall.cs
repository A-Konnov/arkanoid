﻿using UnityEngine;

public class BonusBall : ABonus
{
    public EBallType BallType { get; set; }

    protected override void Activate()
    {
        var balls = GameObject.FindGameObjectsWithTag("Ball");

        foreach (var ball in balls)
        {
            var ballBehaviourExecuter = ball.GetComponent<BallBehaviourExecuter>();
            ballBehaviourExecuter.SetBallType(BallType);
        }
    }

    protected override void Deactivate()
    {
        var balls = GameObject.FindGameObjectsWithTag("Ball");

        foreach (var ball in balls)
        {
            var ballBehaviourExecuter = ball.GetComponent<BallBehaviourExecuter>();
            ballBehaviourExecuter.SetBallType(EBallType.Base);
        }
    }
}
