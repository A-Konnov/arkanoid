﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusMovement : MonoBehaviour
{
    [SerializeField] private float m_movementSpeed = 1f;
    [SerializeField] private float m_rotationSpeed = 60f;
    [SerializeField] private bool m_isMoveUp;


    //опускаме бонус вниз
    private void Start()
    {
        var rb = GetComponent<Rigidbody>();

        if (rb != null && !rb.useGravity)
        {
            var pos = transform.position;
            pos.y = 0;
            transform.position = pos;
        }
    }

    private void Update()
    {
        transform.RotateAround(transform.position, Vector3.up, m_rotationSpeed * Time.deltaTime);

        var moveDir = Vector3.back;

        if (m_isMoveUp)
        {
            moveDir = Vector3.up;
        }

        transform.Translate(moveDir * m_movementSpeed * Time.deltaTime, Space.World);
    }

}
