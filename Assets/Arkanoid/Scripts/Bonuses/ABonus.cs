﻿using System;
using UnityEngine;

public abstract class ABonus : MonoBehaviour
{
    public float Duration { get; set; }
    public bool Infinity { get; set; }

    protected abstract void Activate();
    protected abstract void Deactivate();

    public virtual void Start()
    {
        var type = this.GetType();
        Protect(type);

        Activate();
    }

    private void OnDestroy()
    {
        Deactivate();
    }

    private void Update()
    {
        if (!Infinity)
        {
            Duration -= Time.deltaTime;

            if (Duration <= 0)
            {
                Destroy(this);
            }
        }
    }

    protected void Protect<T>() where T : ABonus
    {
        var comp = GetComponent<T>();

        if (comp != null)
        {
            Destroy(comp);
        }
    }

    protected void Protect(Type type)
    {
        var comps = GetComponents(type);

        for (int i = 0; i < comps.Length - 1; i++)
        {
            DestroyImmediate(comps[i]);
        }
    }
}
