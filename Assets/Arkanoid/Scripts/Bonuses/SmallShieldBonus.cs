﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallShieldBonus : ABonus
{
    protected override void Activate()
    {
        if (gameObject.GetComponent<BigShieldBonus>())
        {
            Destroy(gameObject.GetComponent<BigShieldBonus>());
            var shields = GetComponent<ShieldSwitcher>();
            if (shields != null)
            {
                shields.SwitchShield(false);
            }
        }

        else Deactivate();
    }

    protected override void Deactivate()
    {
        var shields = GetComponent<ShieldSwitcher>();
        if (shields != null)
        {
            shields.SwitchShield(false);
        }
    }
}
