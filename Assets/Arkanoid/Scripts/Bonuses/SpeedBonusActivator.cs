﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedBonusActivator : ABonusActivator
{
    [SerializeField] private float m_speedChange = 2f;
    public float SpeedChange { get; set; }

    private void Start()
    {
        SpeedChange = m_speedChange;
    }

    public override void Activate(GameObject target)
    {
        var bonus = target.AddComponent<SpeedBonus>();
        bonus.Duration = m_duration;
        bonus.SpeedMod = SpeedChange;
    }
}
