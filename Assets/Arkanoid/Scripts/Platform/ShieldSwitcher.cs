﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldSwitcher : MonoBehaviour
{
    [SerializeField] private GameObject m_shield;
    [SerializeField] private GameObject m_shieldColider;

    [SerializeField] private GameObject m_bigShield;
    [SerializeField] private GameObject m_bigShieldColider;

    private void Start()
    {
        SwitchShield(true);
    }



    public void SwitchShield(bool isBig)
    {
        m_shield.gameObject.SetActive(!isBig);
        m_shieldColider.gameObject.SetActive(!isBig);

        m_bigShield.gameObject.SetActive(isBig);
        m_bigShieldColider.gameObject.SetActive(isBig);
    }
}
