﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoPlatformMovement : APlatformMovement
{
    protected override Vector3 CalculateVelocity()
    {
        var ball = GetNearestBall();
        if (ball == null)
        {
            return Vector3.zero;
        }

        var ballRb = ball.GetComponent<Rigidbody>();

        if(ballRb != null && !ballRb.isKinematic)
        {
            var axis = Mathf.Clamp(ball.transform.position.x - transform.position.x, -1, 1);
            return Vector3.right * MovementSpeed * axis;
        }
        return Vector3.zero;
    }

    private Transform GetNearestBall()
    {
        var balls = GameObject.FindGameObjectsWithTag("Ball");
        Transform closesBall = null;
        float closesSqrDistance = float.MaxValue;

        foreach (var ball in balls)
        {
            var distance = (ball.transform.position - transform.position).sqrMagnitude;

            if (distance < closesSqrDistance)
            {
                closesBall = ball.transform;
                closesSqrDistance = distance;
            }
        }

        return closesBall;
    }
}
