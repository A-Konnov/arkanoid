﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManualPlatformMovement : APlatformMovement
{
    protected override Vector3 CalculateVelocity()
    {
        return Vector3.right * MovementSpeed * CustomInput.HorizontalAxis;
    }
}

