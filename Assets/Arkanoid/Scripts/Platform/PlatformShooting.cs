﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlatformShooting : MonoBehaviour
{
    [Header("Settings:")]
    [SerializeField] private float m_pushForce = 5f;

    [Header("References:")]
    [SerializeField] private Rigidbody m_ballPrefab;
    [SerializeField] private Transform m_spawnPoint;
    [SerializeField] private Animator m_animator;

    private Rigidbody m_rbBall;

    private void Start()
    {
        SpawnBall();
        Btn_ThrowBall.throwBall += ThrowBall;
        KeyboardInput.throwBall += ThrowBall;
    }

    private void FixedUpdate()
    {
        if (GameController.StopGame)
        {
            ResetBallOnce();
        }
    }

    private void ThrowBall()
    {
        if (m_rbBall.isKinematic)
        {
            m_animator.SetTrigger("ThrowBall");
        }
    }

    public void ResetBallOnce()
    {
        if (!m_rbBall.isKinematic)
        {
            m_rbBall.isKinematic = true;
            m_rbBall.transform.SetParent(m_spawnPoint);
            m_rbBall.transform.SetPositionAndRotation(m_spawnPoint.position, m_spawnPoint.rotation);            
        }
    }

    private void SpawnBall()
    {
        if (m_ballPrefab != null)
        {
            m_rbBall = Instantiate(m_ballPrefab, m_spawnPoint.position, m_spawnPoint.rotation, m_spawnPoint);
            m_rbBall.isKinematic = true;
        }
    }


    //calling from animation
    private void PushBall()
    {
        if (m_ballPrefab != null)
        {
            m_rbBall.isKinematic = false;
            m_rbBall.transform.parent = null;
            m_rbBall.constraints = RigidbodyConstraints.None;
            m_rbBall.AddForce(m_rbBall.transform.forward * m_pushForce, ForceMode.Impulse);

            GameController.StopGame = false;
        }
    }

    private void OnDisable()
    {
        Btn_ThrowBall.throwBall -= ThrowBall;
        KeyboardInput.throwBall -= ThrowBall;
    }
}
