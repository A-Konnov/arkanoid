﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JsonExample5 : MonoBehaviour
{
    private void Start()
    {

        var character1 = new Character()
        {
            Name = "Anduin",
            Health = 160,
            Level = 3,
            Experience = 156.78f,
            Class = EClass.Mage,
            Test = "TestProperty"
        };

        var character2 = new Character()
        {
            Name = "Tor",
            Health = 250,
            Level = 4,
            Experience = 28.78f,
            Class = EClass.Paladin,
            Test = "TestProperty"
        };

        var character3 = new Character()
        {
            Name = "Gimli",
            Health = 300,
            Level = 2,
            Experience = 318.78f,
            Class = EClass.Warrior,
            Test = "TestProperty"
        };

        var characters = new Dictionary<string, Character>();
        characters.Add("Character1", character1);
        characters.Add("Character2", character2);
        characters.Add("Character3", character3);

        SaveLoad.Save("Characters", characters);
    }
}
