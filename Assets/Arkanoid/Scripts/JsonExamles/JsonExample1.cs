﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;


//jsoneditoronline.org
public class JsonExaple1 : MonoBehaviour
{
    private void Start()
    {
        var character1 = new Character()
        {
            Name = "Anduin",
            Health = 160,
            Level = 3,
            Experience = 156.78f,
            Class = EClass.Mage
        };

        var json = JsonConvert.SerializeObject(character1);
        Debug.Log(json);
    }
}
