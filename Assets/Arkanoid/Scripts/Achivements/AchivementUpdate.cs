﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AchivementUpdate : MonoBehaviour
{
    [SerializeField] private AchivementAsset m_AchivementWin;
    [SerializeField] private AchivementAsset m_AchivementColStars;
    [SerializeField] private AchivementAsset m_AchivementWinGold;

    private void Start()
    {
        EventManager.AddListener("win", AchivementWin);
        EventManager.AddListener("colStars", AchivementColStars);
        EventManager.AddListener("winGold", AchivementWinGold);
    }

    private void OnDestroy()
    {
        EventManager.RemoveListener("win", AchivementWin);
        EventManager.RemoveListener("colStars", AchivementColStars);
        EventManager.RemoveListener("winGold", AchivementWinGold);
    }

    private void AchivementWinGold()
    {
        if (PlayerPrefs.GetInt("LevelStars" + SceneManager.sceneCount) != 3 && GameController.StarCount == 3)
        {
            m_AchivementWinGold.CurrProgress++;
        }
    }

    private void AchivementColStars()
    {
        m_AchivementColStars.CurrProgress++;
    }

    private void AchivementWin()
    {
        m_AchivementWin.CurrProgress++;
    }

}
