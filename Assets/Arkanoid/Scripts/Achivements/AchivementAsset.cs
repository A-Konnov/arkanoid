﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class AchivementAsset : ScriptableObject
{
    [Serializable]
    public struct ProgressStep
    {
        public int StepValue;
        public int RewardValue;
    }

    [SerializeField] private string m_id;
    [SerializeField] private string m_name;
    [SerializeField] private string m_description;
    [SerializeField] private ProgressStep[] m_progressSteps;
    private string m_idRewards;
    private int m_rewards;


    [Header("Debug:")]
    [SerializeField] private int m_debugValue;


    //new
    public string IdRewards
    {
        get { return m_id + "Rewards"; }
    }

    public int Rewards
    {
        get { return PlayerPrefs.GetInt(IdRewards); }
        set { PlayerPrefs.SetInt(IdRewards, Mathf.Clamp(value, 0, m_progressSteps.Length)); }
    }


    //old
    public string Id
    {
        get { return m_id; }
        set { m_id = value; }
    }

    public string Name
    {
        get { return m_name; }
        set { m_name = value; }
    }

    public string Description
    {
        get { return m_description; }
        set { m_description = value; }
    }

    public ProgressStep[] ProgressSteps
    {
        get { return m_progressSteps; }
        set { m_progressSteps = value; }
    }

    public int CurrProgress
    {
        get { return PlayerPrefs.GetInt(m_id); }
        set { PlayerPrefs.SetInt(m_id, Mathf.Clamp(value, 0, MaxReqValue)); }
    }

    public int MaxReqValue
    {
        get
        {
            if (m_progressSteps == null || m_progressSteps.Length == 0)
            {
                return 0;
            }
            return m_progressSteps[m_progressSteps.Length - 1].StepValue;
        }
    }

    public int CurrReqValue
    {
        get
        {
            if (m_progressSteps == null || m_progressSteps.Length == 0)
            {
                return 0;
            }
            foreach (var step in m_progressSteps)
            {
                if (CurrProgress < step.StepValue)
                {
                    return step.StepValue;
                }
            }
            return MaxReqValue;
        }
    }

    public int CompletedStars
    {
        get
        {
            if (m_progressSteps == null || m_progressSteps.Length == 0)
            {
                return 0;
            }

            var stars = 0;
            foreach (var step in m_progressSteps)
            {
                if (CurrProgress >= step.StepValue)
                {
                    stars++;
                    continue;
                }
                break;
            }
            return stars;
        }
    }

    public string GetProgressString()
    {
        return CurrProgress + "/" + CurrReqValue;
    }

    public static void SaveProgress(string id, int value)
    {
        PlayerPrefs.SetInt(id, value);
    }

    [ContextMenu("Save debug progress")]
    public void SaveDebugProgress()
    {
        CurrProgress = m_debugValue;
        Debug.Log("Save");
    }

    [ContextMenu("Reset Reward")]
    public void ResetReward()
    {
        Rewards = 0;
        m_debugValue = 0;
        CurrProgress = m_debugValue;
        Debug.Log("Reset Reward");
    }
}
