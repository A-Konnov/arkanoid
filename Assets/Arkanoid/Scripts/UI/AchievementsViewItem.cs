﻿using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class AchievementsViewItem : MonoBehaviour
{
    [SerializeField] private AchivementAsset m_asset;

    [Header("Reference")]
    [SerializeField] private Text m_name;
    [SerializeField] private Text m_description;
    [SerializeField] private Text m_progress;
    [SerializeField] private Image[] m_stars;
    [SerializeField] private GameObject[] m_RewardsBtn;


    private void Start()
    {
        TryApplyAsset();
    }

#if UNITY_EDITOR
    private void Update()
    {
        TryApplyAsset();
    }
#endif

    private void TryApplyAsset()
    {
        if (m_asset == null)
        {
            return;
        }

        SetName(m_asset.Name);
        SetDescription(m_asset.Description);
        SetProgress(m_asset.GetProgressString());
        SetNumOfStars(m_asset.ProgressSteps.Length);
        SetComletedStars(m_asset.CompletedStars);
        SetRewardBtn(m_asset.CompletedStars, m_asset.Rewards);
    }

    private void SetName(string name)
    {
        if (m_name != null)
        {
            m_name.text = name;
        }
    }

    private void SetDescription(string description)
    {
        if (m_description != null)
        {
            m_description.text = description;
        }
    }

    private void SetProgress(string progress)
    {
        if (m_progress != null)
        {
            m_progress.text = progress;
        }
    }

    private void SetNumOfStars(int num)
    {
        for (int i = 0; i < m_stars.Length; i++)
        {
            m_stars[i].gameObject.SetActive(i < num);
        }
    }

    private void SetComletedStars(int stars)
    {
        var activeStar = Resources.Load<Sprite>("Stars/activeStar");
        var inactiveStar = Resources.Load<Sprite>("Stars/inactiveStar");

        for (int i = 0; i < m_stars.Length; i++)
        {
            if (m_stars[i].gameObject.activeSelf)
            {
                m_stars[i].sprite = i < stars ? activeStar : inactiveStar;
            }
        }
    }

    private void SetRewardBtn(int stars, int rewards)
    {
        for (int i = 0; i < m_stars.Length; i++)
        {
            m_RewardsBtn[i].SetActive(i < stars ? true : false);
        }

        for (int i = 0; i < rewards; i++)
        {
            m_RewardsBtn[i].SetActive(false);
        }
    }

    public void OnGetReward(int rewardStep)
    {
        m_asset.Rewards++;
        Debug.Log("You received " + m_asset.ProgressSteps[rewardStep].RewardValue + " gold coins");
    }
}
