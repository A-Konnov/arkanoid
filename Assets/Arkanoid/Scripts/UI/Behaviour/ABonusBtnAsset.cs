﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ABonusBtnAsset : ScriptableObject
{
    [SerializeField] private Sprite m_icon;
    [SerializeField] private int m_price;
    [SerializeField] private float m_duration;
    [SerializeField] private float m_cooldown;

    private float m_currCooldown = 0f;

    public Sprite Icon
    {
        get { return m_icon; }
    }

    public int Price
    {
        get { return m_price; }
    }

    public float Duration
    {
        get { return m_duration; }
    }

    public float Cooldown
    {
        get { return m_currCooldown; }
    }

    public float CooldownNormalized
    {
        get { return Mathf.Max(0, m_currCooldown / m_cooldown); }
    }

    public virtual void Activate()
    {
        m_currCooldown = m_cooldown;
    }

    public void UpdateCooldown(float dt)
    {
        m_currCooldown = Mathf.Max(0, m_currCooldown - dt); //max из двух чисел
    }
}
