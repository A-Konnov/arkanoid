﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
[RequireComponent(typeof(Button))]
public class BonusActivateBtn : MonoBehaviour
{
    [SerializeField] private ABonusBtnAsset m_bonusAsset;

    [Header("References:")]
    [SerializeField] private Image m_iconImg;
    [SerializeField] private Image m_cooldowmImg;
    [SerializeField] private Text m_priceTxt;

    private void Start()
    {
        ApplyAsset();
        GetComponent<Button>().onClick.AddListener(OnClick);
    }

    private void OnDestroy()
    {
        GetComponent<Button>().onClick.RemoveListener(OnClick);
    }

    private void Update()
    {
#if UNITY_EDITOR
        ApplyAsset();
#endif

        if(!Application.isPlaying)
        {
            return;
        }

        if (m_bonusAsset != null)
        {
            m_bonusAsset.UpdateCooldown(Time.deltaTime);
            ApplyCooldown();
        }
    }

    private void OnClick()
    {
        if (m_bonusAsset != null && m_bonusAsset.CooldownNormalized <= 0)
        {
                        //TODO исправить, чтоб не активился бонус
            if ((int)UserWallet.GetValue(ECurrency.Coin) > m_bonusAsset.Price)
            {
                m_bonusAsset.Activate();
                ChangeCoinAmount();
            }
        }


    }

    private void ChangeCoinAmount()
    {
        var curCoins = UserWallet.GetValue(ECurrency.Coin);
        if (curCoins > m_bonusAsset.Price)
        {
            UserWallet.SetValue(ECurrency.Coin, curCoins - m_bonusAsset.Price);
        }
    }

    private void ApplyAsset()
    {
        if(m_bonusAsset == null || m_iconImg == null || m_priceTxt == null || m_cooldowmImg == null)
        {
            return;
        }

        m_iconImg.sprite = m_bonusAsset.Icon;
        m_priceTxt.text = m_bonusAsset.Price.ToString();
    }

    private void ApplyCooldown()
    {
        m_cooldowmImg.fillAmount = m_bonusAsset.CooldownNormalized;
    }
}
