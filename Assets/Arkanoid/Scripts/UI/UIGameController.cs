﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIGameController : MonoBehaviour
{
    public void MenuBtn()
    {
        SceneManager.LoadSceneAsync(SceneManager.sceneCountInBuildSettings - 1);
    }

    public void ReplayBtn()
    {
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().name);
    }

    public void NextLevelBtn()
    {
        SceneManager.LoadSceneAsync((int)SceneManager.GetActiveScene().buildIndex + 1);
    }
}
