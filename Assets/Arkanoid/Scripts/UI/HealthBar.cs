﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [SerializeField] private Image[] m_health;
    private int m_curHealth;

    private void Start()
    {
        m_curHealth = GameController.PlayerHealth;
    }

    private void Update()
    {
        if (m_curHealth != GameController.PlayerHealth)
        {
            AllHealthDisabled();
            m_curHealth = GameController.PlayerHealth;
            for (int i=0; i< m_curHealth; i++)
            {
                m_health[i].enabled = true;
            }
        }
    }

    private void AllHealthDisabled()
    {
        foreach (Image health in m_health)
        {
            health.enabled = false;
        }
    }

}
