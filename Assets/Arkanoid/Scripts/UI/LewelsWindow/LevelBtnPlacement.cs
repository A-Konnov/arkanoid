﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


public class LevelBtnPlacement : MonoBehaviour
{
    [SerializeField] private LoadLevelBtn m_btnPrefab;
    [SerializeField] private RectTransform m_windows;

    [Serializable]
    public struct LewelWindow
    {
        public Transform Content;
        public int NumOfLevels;
    }

    [SerializeField] private LewelWindow[] m_levelWindows;


    [ContextMenu("Place buttons")]
    public void PlaceButtons()
    {
        m_windows.sizeDelta = new Vector2 (m_windows.transform.childCount * 1000, GetComponent<RectTransform>().anchoredPosition.y);

        var serialNumberLevel = 0;
        foreach (var levelWindow in m_levelWindows)
        {
            while (levelWindow.Content.childCount > 0)
            {
                DestroyImmediate(levelWindow.Content.GetChild(0).gameObject);
            }


            for (int i = 0; i < levelWindow.NumOfLevels; i++)
            {
                var instance = Instantiate(m_btnPrefab, levelWindow.Content);
                serialNumberLevel++;
                instance.LevelNum = serialNumberLevel;
                instance.SetLevelNumTmp(instance.LevelNum);
                instance.SetStars(StarsStorage.GetStars(instance.LevelNum));
                instance.name = "LevelBtn_" + instance.LevelNum;

                instance.gameObject.SetActive(true);
            }
        }
    }
}
