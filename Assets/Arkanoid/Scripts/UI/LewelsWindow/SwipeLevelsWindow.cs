﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwipeLevelsWindow : MonoBehaviour
{
    [SerializeField] private RectTransform m_windows;
    private Vector2 m_startPoint;
    private Vector2 m_curPoint;
    private Vector2 m_startPosition;
    private float m_pitchShift;
    private float m_shearLimit;

    private void Start()
    {
        m_shearLimit = (m_windows.sizeDelta.x / (m_windows.transform.childCount * 2)) * (m_windows.transform.childCount - 1);
        m_windows.anchoredPosition = new Vector2(m_shearLimit, 0);
        m_pitchShift = m_windows.sizeDelta.x / m_windows.transform.childCount;
    }

    public void BeginDrag()
    {
        m_startPosition = m_windows.anchoredPosition;
        m_startPoint = Input.mousePosition;
    }

    public void Drag()
    {
        m_curPoint = Input.mousePosition;
        float x = m_curPoint.x - m_startPoint.x;
        m_windows.anchoredPosition = new Vector2(m_startPosition.x + x, m_startPosition.y);
    }

    public void EndDrag()
    {
        Vector2 m_finishPoint = Input.mousePosition;

        if (m_curPoint.x > m_finishPoint.x && m_startPosition.x > m_shearLimit * -1)
        {
            m_windows.anchoredPosition = new Vector2(m_startPosition.x - m_pitchShift, m_startPosition.y);
            return;
        }
        if (m_curPoint.x < m_finishPoint.x && m_startPosition.x < m_shearLimit)
        {
            m_windows.anchoredPosition = new Vector2(m_startPosition.x + m_pitchShift, m_startPosition.y);
            return;
        }

        m_windows.anchoredPosition = m_startPosition;
    }
}
