﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuAnim : MonoBehaviour
{
    [SerializeField] private Animator[] m_bars;
    [SerializeField] private Animator m_levelsWindow;
    [SerializeField] private Animator m_AchievementWindow;

    [SerializeField] private float m_delay = 1;
    private WaitForSeconds m_wait, m_waitDouble;

    private void Start()
    {
        m_wait = new WaitForSeconds(m_delay);
        m_waitDouble = new WaitForSeconds(m_delay * 2);
        StartCoroutine(PlayAnim("Open"));
    }

    public void OnPlay()
    {
        StartCoroutine(PlayAnim("Close"));
        StartCoroutine(OpenWindow(m_levelsWindow));
    }
    public void OnAchievement()
    {
        StartCoroutine(PlayAnim("Close"));
        StartCoroutine(OpenWindow(m_AchievementWindow));
    }


    public void OnCloseWindow(Animator window)
    {
        window.SetTrigger("Close");
        StartCoroutine(PlayAnim("Open"));
    }

    private IEnumerator PlayAnim(string tag)
    {
        foreach (var bar in m_bars)
        {
            bar.SetTrigger(tag);
            yield return m_wait;            
        }
    }

    private IEnumerator OpenWindow(Animator window)
    {
        yield return m_waitDouble;
        window.SetTrigger("Open");
    }
}
