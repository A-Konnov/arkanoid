﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
//using TMPro;

[RequireComponent(typeof(Button))]
public class LoadLevelBtn : MonoBehaviour
{
    public int LevelNum;

    [SerializeField] private Text m_levelNumTmp;
    [SerializeField] private Image[] m_stars;

    private void Start()
    {     
        SetStars(StarsStorage.GetStars(LevelNum));
        SetLevelNumTmp(LevelNum);

        GetComponent<Button>().onClick.AddListener(OnClick);
    }

    private void OnDestroy()
    {
        GetComponent<Button>().onClick.RemoveListener(OnClick);
    }

    public void SetStars(int num)
    {
        var activeStar = Resources.Load<Sprite>("Stars/activeStar"); //загружаем ассет Sprite из ресурсво
        var inactiveStar = Resources.Load<Sprite>("Stars/inactiveStar");

        for (int i = 0; i< m_stars.Length; i++)
        {
            m_stars[i].sprite = i < num ? activeStar : inactiveStar; //если i < num то activeStar иначе inactiveStar
        }
    }

    public void SetLevelNumTmp(int levelNum)
    {
        if (m_levelNumTmp != null)
        {
            m_levelNumTmp.text = levelNum.ToString();
        }
    }

    private void OnClick()
    {
        if (LevelNum > 0 && LevelNum < SceneManager.sceneCountInBuildSettings - 1)
        {
            SceneManager.LoadSceneAsync(LevelNum);
        }
    }
}
