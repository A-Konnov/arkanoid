﻿using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class StoreItemUI : MonoBehaviour
{
    [SerializeField] private StoreItem m_asset;

    [Header("Reference:")]
    [SerializeField] private Text m_nameTxt;
    [SerializeField] private Text m_amountTxt;
    [SerializeField] private Text m_priceTxt;
    [SerializeField] private Image m_icon;
    [SerializeField] private Button m_buyBtn;

    private void Start()
    {
        if(m_buyBtn != null)
        {
            m_buyBtn.onClick.AddListener(OnClick);
        }
    }

    private void OnDestroy()
    {
        if (m_buyBtn != null)
        {
            m_buyBtn.onClick.RemoveListener(OnClick);
        }
    }

    private void Update()
    {
        if(m_asset != null)
        {
            ApplyAsset();
        }
    }

    private void ApplyAsset()
    {
        m_nameTxt.text = m_asset.Name;
        m_amountTxt.text = "+" + m_asset.GoodAmount;
        m_priceTxt.text = "$" + m_asset.Price;
        m_icon.sprite = m_asset.Icon;
    }

    private void OnClick()
    {
        if(m_asset != null)
        {
            m_asset.Buy();
        }
    }
}
