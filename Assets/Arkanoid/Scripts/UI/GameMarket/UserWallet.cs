﻿using UnityEngine;
using System;

public static class UserWallet
{
    public static event Action<ECurrency> OnItemChanged;

    public static int GetValue(ECurrency currency)
    {
        return PlayerPrefs.GetInt(currency.ToString());
    }

    public static void SetValue(ECurrency currency, int value)
    {
        PlayerPrefs.SetInt(currency.ToString(), value);
        if(OnItemChanged != null)
        {
            OnItemChanged.Invoke(currency);
        }
    }
}
