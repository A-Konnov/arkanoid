﻿using UnityEngine;

[CreateAssetMenu]
public class StoreItem : ScriptableObject
{
    [SerializeField] private string m_name;
    [SerializeField] private Sprite m_icon;

    [Header("Good:")]
    [SerializeField]
    private ECurrency m_goodCurrency;
    [SerializeField] private int m_goodAmount;

    [Header("Price:")]
    [SerializeField]
    private float m_price;

    public string Name
    {
        get { return m_name; }
    }

    public Sprite Icon
    {
        get { return m_icon; }
    }

    public ECurrency Goodcurrency
    {
        get { return m_goodCurrency; }
    }

    public int GoodAmount
    {
        get { return m_goodAmount; }
    }

    public float Price
    {
        get { return m_price; }
    }

    public bool Buy()
    {
        var userValue = UserWallet.GetValue(m_goodCurrency);
        UserWallet.SetValue(m_goodCurrency, userValue + m_goodAmount);
        return true;
    }
}
