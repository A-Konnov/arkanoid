﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "BonusBtnAsset/Wall", fileName = "NewBonusWallAsset")]
public class WallBonusBtnAsset : ABonusBtnAsset
{
    [SerializeField] private EWallType m_type;

    public override void Activate()
    {
        base.Activate();

        var instalWall = FindObjectOfType<InstallWall>();
        if (instalWall == null)
        {
            Debug.LogWarning("There is no component in the scene");
            return;
        }


        switch (m_type)
        {
            case EWallType.Wood:
                {
                    instalWall.InstallAllWoodWall();
                    break;
                }
            case EWallType.Stone:
                {
                    instalWall.InstallAllStoneWall();
                    break;
                }
            case EWallType.Steel:
                {
                    instalWall.InstallAllSteelWall();
                    break;
                }
        }
    }
}
