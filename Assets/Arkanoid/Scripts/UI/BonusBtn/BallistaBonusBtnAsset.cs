﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "BonusBtnAsset/Ballista", fileName = "NewBonusBallistaAsset")]
public class BallistaBonusBtnAsset : ABonusBtnAsset
{
    [SerializeField] private EBallistaType m_type;

    public override void Activate()
    {
        base.Activate();

        var installBallista = FindObjectOfType<BallistaInstall>();
        if (installBallista == null)
        {
            Debug.LogWarning("There is no component in the scene");
            return;
        }

        installBallista.Duration = Duration;

        switch (m_type)
        {
            case EBallistaType.Wood:
                {
                    installBallista.InstallWoodBallista();
                    break;
                }
            case EBallistaType.WoodSteel:
                {
                    installBallista.InstallWoodSteelBallista();
                    break;
                }
            case EBallistaType.Steel:
                {
                    installBallista.InstallSteelBallista();
                    break;
                }
        }
    }
}
