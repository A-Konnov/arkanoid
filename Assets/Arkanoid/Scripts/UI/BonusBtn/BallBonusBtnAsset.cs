﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "BonusBtnAsset/Ball", fileName = "NewBonusBallAsset")]
public class BallBonusBtnAsset : ABonusBtnAsset
{
    [SerializeField] private EBallType m_type;

    public override void Activate()
    {
        base.Activate();

        var platform = GameObject.FindGameObjectWithTag("Platform");
        if (platform == null)
        {
            Debug.LogWarning("There is no Platform in the Scene");
            return;
        }

        var bonus = platform.AddComponent<BonusBall>();
        bonus.Duration = Duration;
        bonus.BallType = m_type;
    }
}
