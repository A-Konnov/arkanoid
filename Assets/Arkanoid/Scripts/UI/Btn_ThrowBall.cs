﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Btn_ThrowBall : MonoBehaviour
{
    public static Action throwBall;

    public void OnButton()
    {
        throwBall();
    }
}
