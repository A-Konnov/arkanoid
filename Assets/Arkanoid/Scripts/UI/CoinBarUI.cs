﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class CoinBarUI : MonoBehaviour
{
    [SerializeField] private Text m_coinAmmountTxt;

    private void Start()
    {
        UpdateCoinAmmount(ECurrency.Coin);
        UserWallet.OnItemChanged += UpdateCoinAmmount;
    }

    private void OnDestroy()
    {
        UserWallet.OnItemChanged -= UpdateCoinAmmount;
    }

    private void UpdateCoinAmmount (ECurrency currency)
    {
        if (currency == ECurrency.Coin)
        {
            m_coinAmmountTxt.text = UserWallet.GetValue(currency).ToString();
        }
    }
}
