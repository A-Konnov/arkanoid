﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class StarsBar : MonoBehaviour
{
    [SerializeField] private Image[] m_stars;
    private int m_curStarsCount;
    private StarSetup[] m_star;

    private void Start()
    {
        ShowStars();
    }

    private void Update()
    {
        if (m_curStarsCount != GameController.StarCount)
        {
            ShowStars();
        }
    }

    public void ShowStars()
    {
        m_curStarsCount = GameController.StarCount;

        for (int i = 0; i < m_curStarsCount; i++)
        {
            m_stars[i].sprite = Resources.Load<Sprite>("Stars/Star-" + i);
        }
    }
}
