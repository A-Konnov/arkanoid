﻿using System.Collections.Generic;
using System;

public class EventManager
{
    private static Dictionary<string, Action> m_events = new Dictionary<string, Action>();

    public static void AddListener(string id, Action listener)
    {
        if(string.IsNullOrEmpty(id) || listener == null)
        {
            return;
        }

        if (!m_events.ContainsKey(id))
        {
            m_events.Add(id, new Action(listener));
        }
        else
        {
            m_events[id] += listener;
        }
    }

    public static void RemoveListener(string id, Action listener)
    {
        if (string.IsNullOrEmpty(id) || listener == null)
        {
            return;
        }

        if (m_events.ContainsKey(id))
        {
            m_events[id] -= listener;
            if(m_events[id].GetInvocationList().Length == 0)
            {
                m_events.Remove(id);
            }
        }
    }

    public static void Invoke(string id)
    {
        if(m_events.ContainsKey(id))
        {
            if (m_events[id] != null)
            {
                m_events[id].Invoke();
            }
        }
    }
}
