﻿using UnityEngine;

public class BonusBarOpen : MonoBehaviour
{
    private Animator m_animator;

    private void Start()
    {
        m_animator = GetComponent<Animator>();
    }

    public void OnClick()
    {
        if (!m_animator.GetBool("OpenBar"))
        {
            OpenBar();
        }
        else
        {
            CloseBar();
        }
    }

    private void OpenBar()
    {
      
        var bars = FindObjectsOfType<BonusBarOpen>();
        foreach (var bar in bars)
        {
            if (bar.m_animator.GetBool("OpenBar"))
            {
                bar.CloseBar();
            }
        }

        m_animator.SetTrigger("Open");
        m_animator.SetBool("OpenBar", true);
    }

    public void CloseBar()
    {
        m_animator.SetTrigger("Close");
        m_animator.SetBool("OpenBar", false);
    }
}
