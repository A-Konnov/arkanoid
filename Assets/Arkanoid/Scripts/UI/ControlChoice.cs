﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;


public class ControlChoice : MonoBehaviour
{
    private TMP_Dropdown m_dropdown;
    private EInputType m_inputType;
    [SerializeField] private InputSettings m_settings;

    private void Start()
    {     
        m_dropdown = GetComponent<TMP_Dropdown>();
        if (m_dropdown != null)
        {
            string[] enumNames = Enum.GetNames(typeof(EInputType));
            List<string> names = new List<string>(enumNames);
            m_dropdown.AddOptions(names);
        }
        else
        {
            Debug.Log("no component");
        }

        m_dropdown.value = (int)m_settings.CurrType;
    }

    public void ValueChanged()
    {
        m_settings.CurrType = (EInputType)m_dropdown.value;
    }
}
