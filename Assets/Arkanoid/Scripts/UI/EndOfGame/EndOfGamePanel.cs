﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndOfGamePanel : MonoBehaviour
{
    [SerializeField] private GameObject m_winDialog;
    [SerializeField] private GameObject m_loseDialog;
    [SerializeField] private Animator m_animator;
    [SerializeField] private GameObject m_shadow;

    private void Start()
    {
        m_shadow.SetActive(false);
        m_winDialog.SetActive(false);
        m_loseDialog.SetActive(false);

        GameController.EndOfGameEvent += OnEndOfGame;
    }

    private void OnDestroy()
    {
        GameController.EndOfGameEvent -= OnEndOfGame;
    }

    private void OnEndOfGame(bool isWin)
    {
        GameController.EndOfGameEvent -= OnEndOfGame;
        m_shadow.SetActive(true);
        m_animator.SetTrigger("Open");
        m_winDialog.SetActive(isWin);
        m_loseDialog.SetActive(!isWin);
    }


}
