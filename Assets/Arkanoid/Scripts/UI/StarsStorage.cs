﻿using UnityEngine;

[CreateAssetMenu]
public class StarsStorage : ScriptableObject
{
    [SerializeField] private int m_levelNum;
    [SerializeField] private int m_starsNum;

    private static string m_key = "LevelStars";

    public static int GetStars(int LevelNum)
    {
        return PlayerPrefs.GetInt(m_key + LevelNum);
    }

    public static void SetStars(int LevelNum, int starsCount)
    {
        PlayerPrefs.SetInt(m_key + LevelNum, starsCount);
    }

    [ContextMenu("Set stars")]
    public void SetStars()
    {
        SetStars(m_levelNum, m_starsNum);
        Debug.Log("Set stars. Curr count: " + GetStars(m_levelNum));
    }
}
