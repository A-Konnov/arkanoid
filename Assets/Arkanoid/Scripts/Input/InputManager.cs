﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    [SerializeField] private InputSettings m_settings;

    private IInput m_input;

    private void Start()
    {
        SetInput();
        m_settings.OnInputChanged.AddListener(SetInput);

//#if UNITY_EDITOR
//        m_input = new KeyboardInput();

        //#elif UNITY_ANDROID
        //        m_input = new MobileInput();
        //#endif

    }

    //передача нового типа управления
    //public void temp()
    //{
    //    m_settings.currtype = einputtype.keyboard;
    //}

    private void OnDestroy()
    {
        m_settings.OnInputChanged.RemoveListener(SetInput);
    }

    private void Update()
    {
        if(m_input != null)
        {
            m_input.UpdateInput();
        }
    }

    private void SetInput()
    {
        switch (m_settings.CurrType)
        {
            case EInputType.Keyboard:
                m_input = new KeyboardInput();
                break;
            case EInputType.Accelerometer:
                m_input = new MobileInput();
                break;
            default:
                m_input = null;
                break;
        }
    }
}
