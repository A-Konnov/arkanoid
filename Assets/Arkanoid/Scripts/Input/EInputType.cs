﻿public enum EInputType
{
    Keyboard = 0,
    Accelerometer = 1,
    UIButtons = 2,
    Finger = 3,
}