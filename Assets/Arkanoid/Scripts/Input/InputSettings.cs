﻿using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName ="InputSettings", fileName = "NewInputSettings")]
public class InputSettings : ScriptableObject
{
    [SerializeField] private EInputType m_currType;

    public UnityEvent OnInputChanged = new UnityEvent();

    public EInputType CurrType
    {
        get { return m_currType; }
        set
        {
            m_currType = value;
            OnInputChanged.Invoke();
        }
    }
}
