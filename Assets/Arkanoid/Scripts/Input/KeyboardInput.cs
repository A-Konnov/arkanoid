﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class KeyboardInput : IInput
{
    public static Action throwBall;

    public void UpdateInput()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            throwBall();
        }

        CustomInput.HorizontalAxis = 0;

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            CustomInput.HorizontalAxis = -1;
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            CustomInput.HorizontalAxis = 1;
        }
    }
}
