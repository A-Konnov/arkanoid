﻿using UnityEngine;

public class MobileInput : IInput
{
    public void UpdateInput()
    {
        CustomInput.HorizontalAxis = Input.acceleration.x * 1.5f;
    }
}
