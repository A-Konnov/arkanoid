﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCMovement : MonoBehaviour
{
    [Header("Property:")]
    [SerializeField] private float m_movementSpeed = 0.5f;
    [SerializeField] private float m_obstacleRange = 0.5f;
    private Animation m_animation;
    
    public float BaseSpeed { get { return m_movementSpeed; } }
    public float MovementSpeed { get; set; }

    private void Start()
    {
        m_animation = GetComponent<Animation>();
        MovementSpeed = BaseSpeed;
    }

    private void Update()
    {
        if (IsRayHit())
        {
            Move();
        }
        else
        {
            Stand();
        }
    }

    private void Stand()
    {
        m_animation.enabled = false;
    }

    private void Move()
    {
        if (m_animation.enabled == false)
        {
            m_animation.enabled = true;
        }
        transform.Translate(Vector3.forward * MovementSpeed * Time.deltaTime * -1);
    }

    private bool IsRayHit()
    {
        Ray ray = new Ray(transform.position, new Vector3(0,0,-1));
        RaycastHit hit;
        if (Physics.SphereCast(ray, 0.75f, out hit))
        {
            GameObject hitObject = hit.transform.gameObject;
            if (hit.distance < m_obstacleRange)
            {
                return false;
            }
        }
        return true;
    }
}
