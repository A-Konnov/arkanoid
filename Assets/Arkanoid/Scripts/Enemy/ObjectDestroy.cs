﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectDestroy : MonoBehaviour
{
    [Header("Referents")]
    [SerializeField]
    private GameObject m_replacingObject;
    [SerializeField] private float m_forceToPieces = 3;
    [SerializeField] private float m_duration = 10;
    [SerializeField] private float m_fallingSpeed = 1;

    private Rigidbody[] m_rb_allChild;

    private void Start()
    {
        if (m_replacingObject != null)
        {
            GameObject newObj = Instantiate(m_replacingObject, transform.position, transform.rotation);
        }

        m_rb_allChild = GetComponentsInChildren<Rigidbody>();

        foreach (Rigidbody rb_child in m_rb_allChild)
        {
            var rnd_vector = new Vector3(Random.Range(-m_forceToPieces, m_forceToPieces), Random.Range(-m_forceToPieces, m_forceToPieces), Random.Range(-m_forceToPieces, m_forceToPieces));
            rb_child.AddForce(rnd_vector * rb_child.mass, ForceMode.Impulse);
        }
    }

    private void Update()
    {
        m_duration -= Time.deltaTime;
        if (m_duration < 5)
        {
            foreach (Rigidbody rb_child in m_rb_allChild)
            {
                Destroy(rb_child);
            }
            transform.Translate(Vector3.down * m_fallingSpeed * Time.deltaTime);
        }

        if (m_duration < 0)
        {
            Destroy(gameObject);
        }
    }
}
