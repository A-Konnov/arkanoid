﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CryptSpawner : MonoBehaviour
{
    [SerializeField] private GameObject m_skeleton;
    [SerializeField] private Transform m_spawn;
    [SerializeField] private GameObject m_fx;
    [SerializeField] private float m_firstDelay = 5f;
    [SerializeField] private float m_delay = 15f;
    [SerializeField] private float m_spawnRange = 2f;

    private void Start()
    {
        StartCoroutine(Spawn());
    }

    private IEnumerator Spawn()
    {
        yield return new WaitForSeconds(m_firstDelay);
        SpawnSkeleton();

        var delay = new WaitForSeconds(m_delay);
        while (true)
        {
            yield return delay;
            SpawnSkeleton();
        }
    }

    private void SpawnSkeleton()
    {
        if (IsRayHit())
        {
            var fx = Instantiate(m_fx, m_spawn.position, m_spawn.rotation);
            Destroy(fx, 3f);
            Instantiate(m_skeleton, m_spawn.position, m_spawn.rotation);
        }
    }

    private bool IsRayHit()
    {
        Ray ray = new Ray(transform.position, new Vector3(0, 0, -1));
        RaycastHit hit;
        if (Physics.SphereCast(ray, 0.75f, out hit))
        {
            GameObject hitObject = hit.transform.gameObject;
            if (hit.distance < m_spawnRange)
            {
                return false;
            }
        }
        return true;
    }
}
