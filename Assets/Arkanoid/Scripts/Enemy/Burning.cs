﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Burning : MonoBehaviour
{
    [SerializeField] private float m_damage = 0.5f;
    [SerializeField] private float m_periodicity = 15f;
    [SerializeField] private float m_burningRadius = 1f;
    [SerializeField] private ParticleSystem m_flame;

    private ObjectsHealth m_health;
    private bool m_burning = false;
    public bool IsBurningStart { get; set; }

    private void Awake()
    {
        m_flame.Stop();
        IsBurningStart = false;
    }

    private void Start()
    {
        m_health = GetComponent<ObjectsHealth>();
    }

    private void Update()
    {
        if (IsBurningStart && !m_burning)
        {
            m_flame.Play();
            IsBurningStart = false;
            m_burning = true;
            StartCoroutine(PeriodicalBurning());
        }
    }

    private IEnumerator PeriodicalBurning()
    {
        var delay = new WaitForSeconds(m_periodicity);
        while (true)
        {
            yield return delay;
            m_health.Health -= m_damage;
            ArsonInRadius();
        }
    }

    private void ArsonInRadius()
    {
        Collider[] firedObjects = Physics.OverlapSphere(gameObject.transform.position, m_burningRadius);
        foreach (var fireObject in firedObjects)
        {
            var burning = fireObject.GetComponent<Burning>();
            if (burning != null)
            {
                burning.IsBurningStart = true;
            }
        }
    }

}
