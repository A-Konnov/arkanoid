﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDestroyEffect : MonoBehaviour
{
    [SerializeField] private GameObject m_fxPrefab;

    public void SpawnFX()
    {

        Vector3 pos = transform.position;
        Quaternion rot = transform.rotation;
        Instantiate(m_fxPrefab, pos, rot);
    }

}
