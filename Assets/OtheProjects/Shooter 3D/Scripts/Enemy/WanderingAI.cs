﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WanderingAI : MonoBehaviour {

    public float speed = 2.0f; //скорость движения
    public float obstacleRange = 1.5f; //расстояние, скоторого начинается реакция на препятствие
    private bool _alive; //переменная для слежения за состоянием персонажа

    void Start()
    {
        _alive = true; //инициализация переменной
    }

	void Update ()
    {
        if (_alive) //движение начинается только в случае живого персонажа
        {
            transform.Translate(0, 0, speed * Time.deltaTime); //Непрерывное движение вперед в каждом кадре не сморя на повороты
            Ray ray = new Ray(transform.position, transform.forward); //Луч находится в том же положении и нацеливается в том же направлении, что и персонаж
            RaycastHit hit;
            if (Physics.SphereCast(ray, 0.75f, out hit)) //Бросаем луч с описанной вокруг него окружностью.
            {
                GameObject hitObject = hit.transform.gameObject;
                if (hit.distance < obstacleRange)
                {
                    float angle = Random.Range(100, 260); //случайный выбор нового направления
                    transform.Rotate(0, angle, 0); //задаем направление
                }
            }
        }
	}

    public void SetAlive (bool alive) //открытый метод, позволяющий внешенму коду воздействовать на "живое" состояние
    {
        _alive = alive;
    }
}
