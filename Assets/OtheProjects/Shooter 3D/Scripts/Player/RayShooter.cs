﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayShooter : MonoBehaviour
{
    [SerializeField] private Camera _camera;

    public GameObject[] Bullet; //0-red, 1-green, 2-blue
    public GameObject[] SignalBullet;//сигнальные шары
    public Transform ShootPoint;
    public float ShootSpeed;
    public float ShootPower;

    GameObject currentSignalBullet;

    int curentBulletType;
    int prevBulletType;
    [SerializeField] int bulletMax;
    int bullets;

    float nextShotTimer;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked; //блокировка указателя мыши по центру
        Cursor.visible = false; //скрываем указатель мыши

        curentBulletType = 0;
        bullets = bulletMax;
    }

    void OnGUI()
    {
        int size = 50;
        float posX = _camera.pixelWidth / 2 - size / 4;
        float posY = _camera.pixelHeight / 2 - size / 2;
        GUI.Label(new Rect(posX, posY, size, size), "+"); //Команда GUI.Label отображает символ на экране
    }

    private void FixedUpdate()
    {

        //стрельба
        if (Input.GetButton("Fire1") && Time.time > nextShotTimer && bullets > 0)
        {
           // Vector3 pos = new Vector3(_camera.pixelWidth / 2, _camera.pixelHeight / 2, 0);
            GameObject obj = Instantiate(Bullet[curentBulletType], ShootPoint.position, Quaternion.identity);
            obj.GetComponent<Rigidbody>().AddForce(transform.forward * ShootPower, ForceMode.Impulse);
            nextShotTimer = Time.time + ShootSpeed;
            bullets--;
            Debug.Log(bullets);
        }

        //смена типа шара
        if (Input.GetKeyDown(KeyCode.Alpha1)) curentBulletType = 0;
        if (Input.GetKeyDown(KeyCode.Alpha2)) curentBulletType = 1;
        if (Input.GetKeyDown(KeyCode.Alpha3)) curentBulletType = 2;

        //смена сигнального шара
        if (curentBulletType != prevBulletType || currentSignalBullet == null)
        {
            Destroy(currentSignalBullet);
            currentSignalBullet = Instantiate(SignalBullet[curentBulletType], ShootPoint.position, ShootPoint.rotation) as GameObject;
            currentSignalBullet.transform.parent = transform;
        }
        prevBulletType = curentBulletType;
    }

    private void OnTriggerEnter(Collider bullet)
    {
        if (bullet.gameObject.CompareTag("Bullet") && bullets < bulletMax)
        {
            bullets++;
            Debug.Log(bullets);
            Destroy(bullet.gameObject);
        }

    }

}