﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Shooter3DVictory : MonoBehaviour
{
    private void Update()
    {
        if (GameObject.FindWithTag("Enemy") == null)
        {
            Debug.Log("Victory");
            Scene activeScene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(activeScene.name);
        }
    }
}
