﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour {
    public enum RotationAxes //Объявляем структуру данных enum, которая будет сопоставлять имена с параметрами.
    {
        MoveMouseXAndY = 0,
        MoveMouseX = 1,
        MoveMouseY = 2
    }
    public RotationAxes axes = RotationAxes.MoveMouseXAndY; // Объявляем общедоступную переменную, которая появится в редакторе Unity.
    public float speedRotateHorizont = 9.0f; //Объявляем переменную для скорости вращения
    public float speedRotateVertikal = 9.0f; //Объявляем переменные, задающие поворот в вертикальной плоскости

    public float minimumVert = -30.0f; //Пределы угла наклона
    public float maximumVert = 30.0f;

    private float _angleVertikal = 0; //Объявляем закрытую переменную для угла поворота по вертикали.
    private float _angleHorizont = 0;

    // Use this for initialization
    void Start () {
        Rigidbody body = GetComponent<Rigidbody>();
        if (body != null) // Проверяем, существует ли этот компонент.
        body.freezeRotation = true;
    }
	
	// Update is called once per frame
	void Update () {
        if (axes == RotationAxes.MoveMouseX)
        {
            // это поворот в горизонтальной плоскости. Просто впишим новое значение угла из мыши
            transform.Rotate(0, Input.GetAxis("Mouse X") * speedRotateHorizont, 0);
        }
        else if (axes == RotationAxes.MoveMouseY)
        {
            // это поворот в вертикальной плоскости
            /*
             * Получаем от мыши угол поврота по оси X
             * Ограничиваем угол пределами
             * Записываем текущее значение угла по оси Y (не вращяем по ней)
             * Записываем новый Vector3 из созданных значений X и Y
            */
            _angleVertikal -= Input.GetAxis("Mouse Y") * speedRotateVertikal;
            _angleVertikal = Mathf.Clamp(_angleVertikal, minimumVert, maximumVert);
            float rotationY = transform.localEulerAngles.y;
            transform.localEulerAngles = new Vector3(_angleVertikal, rotationY, 0);
        }
        else
        {
            // это комбинированный поворот
            _angleVertikal -= Input.GetAxis("Mouse Y") * speedRotateVertikal;
            _angleVertikal = Mathf.Clamp(_angleVertikal, minimumVert, maximumVert);

            _angleHorizont += Input.GetAxis("Mouse X") * speedRotateHorizont;
            
            transform.localEulerAngles = new Vector3(_angleVertikal, _angleHorizont, 0);
        }

    }
}
