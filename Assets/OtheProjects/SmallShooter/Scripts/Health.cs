﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour
{
    [SerializeField] private int m_hp;
    [SerializeField] private UnityEvent m_onHpEndEvent;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            m_hp--;

            if (m_hp <=0)
            {
                Destroy(gameObject);
                m_onHpEndEvent.Invoke();
            }
        }
    }
}
