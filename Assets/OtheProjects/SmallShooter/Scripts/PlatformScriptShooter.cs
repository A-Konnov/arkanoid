﻿using UnityEngine;
using System.Collections;

public class PlatformScriptShooter : MonoBehaviour
{
    public GameObject[] Bullet; //0-red, 1-green, 2-blue
    public GameObject[] SignalBullet;//сигнальные шары
    public Transform ShootPoint;
    public float ShootSpeed;
    public float ShootPower;

    GameObject currentSignalBullet;

    int curentBulletType;
    int prevBulletType;
    int bullets;

    float nextShotTimer;

    private void Start()
    {
        curentBulletType = 0;
        bullets = 10;
    }

    private void FixedUpdate()
    {
        //двигаем обьект вперед
        transform.Translate(Vector3.forward * 0.05f, Space.World);

        //движение влево/вправо
        float horizontalInput = Input.GetAxis("Mouse X");
        if (horizontalInput < 0 && transform.position.x > -3.8f)
        {
            transform.Translate(0.3f * horizontalInput, 0, 0, Space.World);
        }

        if (horizontalInput > 0 && transform.position.x < 3.8f)
        {
            transform.Translate(0.3f * horizontalInput, 0, 0, Space.World);
        }

        //стрельба
        if (Input.GetButton("Fire1") && Time.time > nextShotTimer && bullets > 0)
        {
            GameObject obj = Instantiate(Bullet[curentBulletType], ShootPoint.position, ShootPoint.rotation) as GameObject;
            obj.GetComponent<Rigidbody>().AddForce(Vector3.forward * ShootPower, ForceMode.Impulse);
            //Destroy(obj, 5f); //удаляем пулю
            nextShotTimer = Time.time + ShootSpeed;
            bullets--;
            Debug.Log(bullets);
        }

        //смена типа шара
        if (Input.GetKeyDown(KeyCode.Alpha1)) curentBulletType = 0;
        if (Input.GetKeyDown(KeyCode.Alpha2)) curentBulletType = 1;
        if (Input.GetKeyDown(KeyCode.Alpha3)) curentBulletType = 2;

        //смена сигнального шара
        if (curentBulletType != prevBulletType || currentSignalBullet == null)
        {
            Destroy(currentSignalBullet);
            currentSignalBullet = Instantiate(SignalBullet[curentBulletType], ShootPoint.position, ShootPoint.rotation) as GameObject;
            currentSignalBullet.transform.parent = transform;
        }
        prevBulletType = curentBulletType;
    }

    private void OnTriggerEnter(Collider bullet)
    {
        if (bullet.gameObject.CompareTag("Bullet"))
        {
            if (bullets<10)
            bullets++;
            Destroy(bullet.gameObject);
        }

    }
}
