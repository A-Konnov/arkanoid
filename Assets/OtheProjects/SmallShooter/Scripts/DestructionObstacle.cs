﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructionObstacle : MonoBehaviour
{
    [SerializeField] private float m_destroySpeed = 2f;

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name != "Ground")
        {
            Destroy(gameObject, m_destroySpeed);
        }

    }

}
