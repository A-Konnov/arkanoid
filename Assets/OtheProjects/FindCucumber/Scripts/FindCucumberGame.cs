﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;
using UnityEngine.UI;

public class FindCucumberGame : MonoBehaviour
{
    [Space(10)]
    [Header("Setting:")]
    [SerializeField] private int m_numOfCucumbers = 3;
    [SerializeField] private int m_numOfObjects = 10;
    [SerializeField] private float m_gameLevelTimer = 10;

    [Header("Prefabs:")]
    [SerializeField] private GameObject m_basePrefab;

    [Header("UI:")]
    [SerializeField] private Text m_UILevelTimer;
    [SerializeField] private Text m_UICucumberLeft;
    [SerializeField] private Text m_UIGameLevel;
    [SerializeField] private Text m_UIGamePointCounter;

    private bool m_isVictory;
    private int m_currNumOfObjects;
    private GameObject[] m_allObjects;
    private float m_reloadLevelTimer;
    private float m_currGameLevelTimer;
    private int m_currNumOfCucumbers;
    private int m_gamePoints = 0;
    private int m_destroyedCucumbers = 0;
    private int m_gameLevel = 1;


    private void Start()
    {
        m_currNumOfCucumbers = m_numOfCucumbers;
        m_currGameLevelTimer = m_gameLevelTimer;
        m_currNumOfObjects = m_numOfObjects;
        BuildCucumberScene();
    }


    private void Update()
    {
        if (!m_isVictory)
        {
            GameLevelTimer();
            DestroyObjectOnClick();

            if (IsEndOfGame())
            {
                FallObjects();
                m_reloadLevelTimer = Time.time + 1.5f;
            }
        }
        else
        {
            if (Time.time > m_reloadLevelTimer)
            {
                m_currNumOfObjects++;
                DestroyAllObjects();
                m_currNumOfCucumbers = (int)m_currNumOfObjects / 5 + 1; //увеличиваем кол-во огурцов каждые 5 уровней
                BuildCucumberScene();
                ResetVictoryState();
                m_currGameLevelTimer = m_gameLevelTimer;
                m_destroyedCucumbers = 0;
                m_gameLevel++;
                m_UIGameLevel.GetComponent<Text>().text = m_gameLevel + " Level";
                Debug.Log("огурцов: " + m_currNumOfCucumbers);
            }
        }
    }

    private void GameLevelTimer()
    {
        m_currGameLevelTimer = m_currGameLevelTimer - Time.deltaTime;
        m_UILevelTimer.GetComponent<Text>().text = "Time to End: " + Math.Round(m_currGameLevelTimer);

        if (m_currGameLevelTimer < 0) //Перезапуск игры, если таймер 0
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

    private void ResetVictoryState()
    {
        m_isVictory = false;
    }

    private void DestroyAllObjects()
    {
        for (int i = 0; i < m_allObjects.Length; i++)
        {
            if (m_allObjects[i] != null)
            {
                Destroy(m_allObjects[i]);
            }
        }
    }

    private bool IsEndOfGame()
    {
        int deletedObjects = 0;
        int numOffCucumbers = 0;

        for (int i=0; i<m_allObjects.Length; i++)
        {
            GameObject currObj = m_allObjects[i];
            if (currObj == null)
            {
                deletedObjects++;
            }
            else if (currObj.GetComponent<MeshRenderer>().material.color == Color.green)
            {
                numOffCucumbers++;
            }
        }

        if (numOffCucumbers == 0 || deletedObjects > (m_currNumOfCucumbers - 1))
        {
            m_isVictory = CheckVictoryState(numOffCucumbers);
            return true;
        }
        return false;
    }

    private bool CheckVictoryState(int numOffCucumbers)
    {
        if(numOffCucumbers > 1)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            return false;
        }
        else
        {
            return true;
        }
    }

    private void BuildCucumberScene()
    {
        m_allObjects = new GameObject[m_currNumOfObjects];
        Color[] colors = new Color[] { Color.red, Color.blue, Color.yellow, Color.black, Color.gray };
        int objIndex = 0;
        int cucumber = m_currNumOfCucumbers;
        int otherObjects = m_currNumOfObjects - m_currNumOfCucumbers;
        int iteration = 30;
        int nullObj = 0;

        Debug.Log("Количество шаров: " + m_currNumOfObjects);
        m_UICucumberLeft.GetComponent<Text>().text = "Cucumbers left: " + m_currNumOfCucumbers;

        while (cucumber > 0)
        {
            if (TryCreateObject(Color.green, objIndex))
            {
                cucumber--;
                objIndex++;
            }

        }

        while (otherObjects > 0 && iteration > 0) //Избавляемся от бесконечности
        {
            int rndIndex = Random.Range(0, colors.Length);
            if (TryCreateObject(colors[rndIndex], objIndex))
            {
                otherObjects--;
                objIndex++;
            }
            else
            {
                iteration--;
                Debug.Log(iteration);
            }
        }

        for (int i = 0; i<m_allObjects.Length; i++) //Считаем кол-во null объектов
        {
            if (m_allObjects[i] == null)
            {
                nullObj++;
            }
        }

        if (nullObj != 0) //Если есть null объекты, то изменяем размер массива
        {
            Array.Resize(ref m_allObjects, m_allObjects.Length - nullObj);
        }

        Debug.Log("Шаров на поле: " + m_allObjects.Length);
    }

    private bool TryCreateObject(Color clr, int objIndex)
    {
        float posX = Random.Range(-4, 5) + Random.Range(-0.15f, 0.15f);
        float posY = Random.Range(-4, 5) + Random.Range(-0.15f, 0.15f);

        Vector3 pos = new Vector3(posX, posY, 0);
        Quaternion rot = Random.rotation;

        for (int i = 0; i < m_allObjects.Length; i++)
        {
            GameObject currObj = m_allObjects[i];
            if (currObj != null && Vector3.Distance(pos, currObj.transform.position) < 1f)
            {
                return false;
            }
        }

        GameObject instance = Instantiate(m_basePrefab.gameObject, pos, rot);
        instance.GetComponent<MeshRenderer>().material.color = clr;
        m_allObjects[objIndex] = instance;

        return true;
    }
    
    private void DestroyObjectOnClick()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 50f))
            {
                GameObject currGameObject = hit.collider.gameObject;
                if (currGameObject.GetComponent<MeshRenderer>().material.color == Color.green)
                {
                    m_destroyedCucumbers++;
                    m_UICucumberLeft.GetComponent<Text>().text = "Cucumbers left: " + (m_currNumOfCucumbers - m_destroyedCucumbers);
                    m_gamePoints++;
                    m_UIGamePointCounter.GetComponent<Text>().text = "" + m_gamePoints;
                }
                Destroy(currGameObject);
            }
        }
    }

    private void FallObjects()
    {
        for (int i=0; i<m_allObjects.Length; i++)
        {
            if(m_allObjects[i] != null)
            {
                m_allObjects[i].GetComponent<Rigidbody>().isKinematic = false;
            }
        }
    }
}
