﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxJumping : MonoBehaviour
{
    public Rigidbody[] m_boxes;
    public Rigidbody m_selectBox;

    private void FixedUpdate()
    {
        SelectBox();

        if (m_selectBox == null)
        {
            return;
        }

        //Jump
        if (Input.GetKeyDown(KeyCode.Space))
        {
            m_selectBox.AddForce(Vector3.up * 200);
        }

        //Move forvard
        if (Input.GetKeyDown(KeyCode.UpArrow)) 
        {
            m_selectBox.AddForce(Vector3.forward * 100);
        }

        //Move backward
        if (Input.GetKeyDown(KeyCode.DownArrow)) 
        {
            m_selectBox.AddForce(Vector3.back * 100);
        }

        //Move left
        if (Input.GetKeyDown(KeyCode.LeftArrow)) 
        {
            m_selectBox.AddForce(Vector3.left * 100);
        }

        //Move right
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            m_selectBox.AddForce(Vector3.right * 100);
        }
    }

    private void SelectBox()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1)) //детектить нажате кнопки 1
        {
            m_selectBox = m_boxes[0];
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            m_selectBox = m_boxes[1];
        }

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            m_selectBox = m_boxes[2];
        }

        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            m_selectBox = m_boxes[3];
        }

        if(m_selectBox != null)
        {
            Debug.Log("Select box: " + m_selectBox.name);
        }
        
    }
}
