﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockBuilder : MonoBehaviour
{
    private enum Action
    {
        None = -1,
        Build = 0,
        Destroy = 1
    }

    private BlockSelector m_selector;
    private GameObject m_mainCube;

    void Start()
    {
        m_selector = GetComponent<BlockSelector>();
        m_mainCube = GameObject.FindWithTag("MAINCUBE");
        //m_mainCube = GameObject.Find("MAINCUBE");
    }

    void Update()
    {
        Action action = Action.None;

        if (Input.GetMouseButtonUp(0))
        {
            action = Action.Build;
        }
        else if (Input.GetMouseButtonUp(1))
        {
            action = Action.Destroy;
        }

        if (action == Action.None) return;

        Vector3 inputRay = new Vector3(Camera.main.pixelWidth / 2, Camera.main.pixelHeight / 2);
        Ray ray = Camera.main.ScreenPointToRay(inputRay);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 10f))
        {
            GameObject targetBlock = hit.collider.gameObject;

            if(action == Action.Build)
            {
                Vector3 pos = FindBlockPos(targetBlock.transform.position, hit.point);
                CreateBlock(m_selector.SelectedBlock, pos, m_mainCube.transform);
            }
            else
            {
                Destroy(targetBlock);
            }
        }

    }

    private void CreateBlock(GameObject prefab, Vector3 pos, Transform parent)
    {
        if (Vector3.Distance(transform.position, pos) > 1.5f)
        {
            GameObject instance = Instantiate(prefab, pos, Quaternion.identity, parent);
            instance.transform.localScale = new Vector3(1f, 1f, 1f);
        }
        else
        {
            Debug.Log("TOO CLOSE!");
        }
    }

    private Vector3 FindBlockPos(Vector3 boxPoint, Vector3 hitPoint)
    {
        float x = CalcTranslationPoint(boxPoint.x, hitPoint.x);
        float y = CalcTranslationPoint(boxPoint.y, hitPoint.y);
        float z = CalcTranslationPoint(boxPoint.z, hitPoint.z);
        Vector3 translation = new Vector3(x, y, z);
        Vector3 result = boxPoint + translation; ;
        return result;
    }

    private int CalcTranslationPoint(float p1, float p2)
    {
        int result = 0;

        if(Mathf.Abs(p1 - p2) > 0.45f)
        {
            if (p1 > p2)
            {
                result = -1;
            }
            else if (p1 < p2)
            {
                result = 1;
            }
        }

        return result;
    }
}
