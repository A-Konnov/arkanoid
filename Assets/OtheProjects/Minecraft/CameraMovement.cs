﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField] private float m_rotationSpeed;
    [SerializeField] private float m_movementSpeed;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked; //блокировка указателя мыши по центру
        Cursor.visible = false; //скрываем указатель мыши
    }

    void Update()
    {
        float dt = Time.deltaTime;
        DoRotation(dt);
        DoMovement(dt);
    }

    void OnGUI()
    {
        int size = 50;
        float posX = Camera.main.pixelWidth / 2;
        float posY = Camera.main.pixelHeight / 2;
        GUI.Label(new Rect(posX, posY, size, size), "+"); //Команда GUI.Label отображает символ на экране
    }

    private void DoMovement(float dt)
    {
        float mSpeed = m_movementSpeed * dt;

        float hSpeed = Input.GetAxis("Horizontal") * mSpeed;
        float fSpeed = Input.GetAxis("Straight") * mSpeed;
        float vSpeed = Input.GetAxis("Vertical") * mSpeed;

        //Move left/right
        transform.Translate(transform.right * hSpeed, Space.World);

        //Move forvard/back
        transform.Translate(transform.forward * fSpeed, Space.World);

        //Move up/down
        transform.Translate(transform.up * vSpeed, Space.World);
    }

    private void DoRotation(float dt)
    {
        float xRotSpeed = Input.GetAxis("Mouse X") * m_rotationSpeed * dt;
        float yRotSpeed = Input.GetAxis("Mouse Y") * m_rotationSpeed * dt;

        transform.RotateAround(transform.position, Vector3.up, xRotSpeed);
        transform.RotateAround(transform.position, transform.right, -yRotSpeed);
    }
}
